/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import edu.jsu.oss.android.map.CampusMap;
import edu.jsu.oss.android.rss.RssNewsReader;
import edu.jsu.oss.android.scoreboard.ScoreboardListLoadingTask;

public class jsu_mobile extends Activity {
    
	private GridView mainMenu;
	private Context context;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        context = this;
        setContentView(R.layout.main);
        
        mainMenu = (GridView)findViewById(R.id.MainMenuGridView);
        mainMenu.setPadding(5, 5, 5, 5);
        mainMenu.setAdapter(new MainMenuImageAdapter(this));
        mainMenu.setOnItemClickListener(iconSelectionListener);
    }
    
    private OnItemClickListener iconSelectionListener = new OnItemClickListener() {
    	
    	@SuppressWarnings("unchecked")
		@Override
    	public void onItemClick(AdapterView<?> parent, View v, int position, long id){
    		//Needs to be called to fix disappearing item[0] bug
    		//ensures all controls are drawn and not discarded after an event,
    		//so when application returns all controls will be present with proper values
    		mainMenu.requestLayout();
    		
    		//Starts activity based on the icon clicked
    		switch (position){
			case 0:
				startActivity(new Intent(getApplicationContext(), CampusMap.class));
				break;
			case 1: //TODO make the string and int finals here
				Intent chantyIntent = new Intent(getApplicationContext(), RssNewsReader.class);
				chantyIntent.putExtra("link", "http://www.thechanticleeronline.com/articles.rss");
				chantyIntent.putExtra("feedType", 1);
				startActivity(chantyIntent);
				break;
			case 2:
				new ScoreboardListLoadingTask(context).execute();
				break;
			case 3:
				Intent newsIntent = new Intent(getApplicationContext(), RssNewsReader.class);
				newsIntent.putExtra("link", "http://feeds2.feedburner.com/jsunews");
				newsIntent.putExtra("feedType", 0);
				startActivity(newsIntent);
				break;
		}
    	}
    };
}