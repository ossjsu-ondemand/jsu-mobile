/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MainMenuImageAdapter extends BaseAdapter {
    private Context mContext;

    public MainMenuImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return MainMenu.getLength();
    }

    //does nothing
    public Object getItem(int position) {
        return null;
    }

    //does nothing
    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        
    	View v;
        
        if (convertView == null) {  // if it's not recycled, initialize some attributes
        	//set layout inflater and get the view from the xml file
        	LayoutInflater li = LayoutInflater.from(mContext);
			v = li.inflate(R.layout.icontext, null);
			
			//set icon image
			ImageView iv = (ImageView)v.findViewById(R.id.icon_image);
			iv.setImageResource(MainMenu.getIcon(position));
			
			//set icon text
			TextView tv = (TextView)v.findViewById(R.id.icon_text);
			tv.setText(MainMenu.getText(position));
        } else {
        	v = convertView;
        }
        
        return v;
    }
}