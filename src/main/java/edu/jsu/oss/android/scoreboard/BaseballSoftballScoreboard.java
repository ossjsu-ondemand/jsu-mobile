/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import edu.jsu.oss.android.R;

public class BaseballSoftballScoreboard extends GamecockScoreboard {

	private final int runsAway = 6;
	private final int hitsAway = 7;
	private final int errorsAway = 8;
	private final int runsHome = 9;
	private final int hitsHome = 10;
	private final int errorsHome = 11;
	private final int outsNumber = 12;
	
	@Override
    public synchronized void onCreate(Bundle savedInstanceState) {
		contentView = R.layout.scoreboard_baseball_softball;
		
		super.onCreate(savedInstanceState);
        
        ImageView logoView = (ImageView) findViewById(R.id.baseball_softball_logo);
        
        if(sport == Sports.baseball){
        	logoView.setImageDrawable(getResources().getDrawable(R.drawable.sport_baseball_logo));
        	sport = Sports.baseball;
        }
        else{
        	logoView.setImageDrawable(getResources().getDrawable(R.drawable.sport_softball_logo));
        	sport = Sports.softball;
        }   
	}
	
	protected GamecockUpdater getNewUpdater(){
		return new BaseballSoftballUpdater(scoreUpdateHandler, refreshRate ,sport);
	}
	
	protected Object getViewData(){
		Object[] data = (Object[])super.getViewData();
		
		data[runsAway] = getViewText(R.id.baseball_softball_runs_away);
		data[hitsAway] = getViewText(R.id.baseball_softball_hits_away);
		data[errorsAway] = getViewText(R.id.baseball_softball_errors_away);
		data[runsHome] = getViewText(R.id.baseball_softball_runs_home);
		data[hitsHome] = getViewText(R.id.baseball_softball_hits_home);
		data[errorsHome] = getViewText(R.id.baseball_softball_errors_home);
		data[outsNumber] = getViewText(R.id.baseball_softball_outs_number);
		
		return data;
	}
	
	protected void rotationQuickload(final Object[] data){
		super.rotationQuickload(data);
		
		setTextView((String)data[runsAway], R.id.baseball_softball_runs_away);
		setTextView((String)data[hitsAway], R.id.baseball_softball_hits_away);
		setTextView((String)data[errorsAway], R.id.baseball_softball_errors_away);
		setTextView((String)data[runsHome], R.id.baseball_softball_runs_home);
		setTextView((String)data[hitsHome], R.id.baseball_softball_hits_home);
		setTextView((String)data[errorsHome], R.id.baseball_softball_errors_home);
		setTextView((String)data[outsNumber], R.id.baseball_softball_outs_number);
	}
	
	protected void digitizeView(){
        //set custom font for digital look - outs, runs, hits, errors
        digitizeFont((TextView)findViewById(R.id.baseball_softball_outs_number));
        digitizeFont((TextView)findViewById(R.id.baseball_softball_runs_home));
        digitizeFont((TextView)findViewById(R.id.baseball_softball_runs_away));
        digitizeFont((TextView)findViewById(R.id.baseball_softball_hits_home));
        digitizeFont((TextView)findViewById(R.id.baseball_softball_hits_away));
        digitizeFont((TextView)findViewById(R.id.baseball_softball_errors_home));
        digitizeFont((TextView)findViewById(R.id.baseball_softball_errors_away));
        
        //If the display is sideways, fill in the additional blanks
        if(getWindowManager().getDefaultDisplay().getOrientation() == Configuration.ORIENTATION_PORTRAIT){
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_home_inning1));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_home_inning2));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_home_inning3));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_home_inning4));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_home_inning5));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_home_inning6));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_home_inning7));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_home_inning8));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_home_inning9));
        	
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_away_inning1));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_away_inning2));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_away_inning3));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_away_inning4));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_away_inning5));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_away_inning6));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_away_inning7));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_away_inning8));
        	digitizeFont((TextView)findViewById(R.id.baseball_softball_score_away_inning9));
        }
        else //set the inning number when necessary
            digitizeFont((TextView)findViewById(R.id.baseball_softball_innings_number));      
	}
}
