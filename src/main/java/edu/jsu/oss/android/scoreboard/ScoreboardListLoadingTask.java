/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

@SuppressWarnings("rawtypes")
public class ScoreboardListLoadingTask extends AsyncTask {

	Context context;
	Bundle bundle;
	Intent scoreboardListIntent;
	ProgressDialog waitDialog;
	
	//Get the context and the bundle for starting the next intent
	public ScoreboardListLoadingTask(Context context){
		this.context = context;
	}
	
	//Before we start retrieving the details activity, show the loading dialog
	@Override 
	protected void onPreExecute() {
		waitDialog = ProgressDialog.show(context, "Retrieving scoreboard data", "Please wait...");
	}
	
	//While the dialog is displayed, gather the information
	@Override
	protected Object doInBackground(Object... params) {
		
		GamesInProgress inProgress = new GamesInProgress();
		
		bundle = new Bundle();
		//bundle.putBooleanArray("inSeason", inProgress.inSeason);
		bundle.putBooleanArray("gameComplete", inProgress.gameComplete);
		bundle.putIntArray("timeUnits", inProgress.timeUnitsElapsed);
				
		scoreboardListIntent = new Intent(context, ScoreboardListView.class);
		scoreboardListIntent.putExtras(bundle);
		context.startActivity(scoreboardListIntent);
		
		return null;
	}

	//When the activity has started, dismiss the wait dialog 
	@Override
	protected void onPostExecute(Object result) {
		waitDialog.dismiss();		
	}
}
