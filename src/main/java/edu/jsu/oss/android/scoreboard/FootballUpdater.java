/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.os.Handler;
import edu.jsu.oss.android.R;

/**
 * @author Josh
 *
 */
public class FootballUpdater extends GamecockUpdater {
	
	public FootballUpdater(Handler scoreboardMessageHandler, int refreshTime, int sport) {
		super(scoreboardMessageHandler, refreshTime, sport);
		
		parserHandler = new DefaultHandler(){
			
			private boolean homeTeam;
			
			private boolean homeHasBall;
			private String homeId;
			
			//todo strings should start as null
			private String clock = "";
			private String ballOn = "";
			private String down = "";
			private String toGo = "";
			
			public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
							
				if(localName.equalsIgnoreCase("venue")){
					homeId = attributes.getValue("homeid");
				}
				if(localName.equalsIgnoreCase("play")){
					clock = attributes.getValue("clock");
					ballOn = attributes.getValue("spot");
					down = attributes.getValue("down");
					toGo = attributes.getValue("togo");
					
					if(attributes.getValue("hasball").equalsIgnoreCase(homeId))
						homeHasBall = true;
					else
						homeHasBall = false;
						
				}
				if(localName.equalsIgnoreCase("linescore")){					
					if(homeTeam){
						sendMessageBundle(makeTwoDigits(attributes.getValue("score")), R.id.football_home_score);
						sendMessageBundle(attributes.getValue("prds"), R.id.football_quarter);
					}
					else
						sendMessageBundle(makeTwoDigits(attributes.getValue("score")), R.id.football_away_score);
				}
				if(localName.equalsIgnoreCase("team")){
					if(attributes.getValue("vh").equalsIgnoreCase("V"))
						homeTeam = false;
					else
						homeTeam = true;
				}
			}
			
			public void endDocument(){ 
				//These are read sequentially, so when the end is reached we know that the 
				//correct fields are present, so update the views accordingly.
				sendMessageBundle(clock, R.id.football_clock);
				sendMessageBundle(getBallOn(ballOn), R.id.football_ball_on);
				sendMessageBundle(down, R.id.football_down);
				sendMessageBundle(makeTwoDigits(toGo), R.id.football_to_go);
				
				if(homeHasBall){
					sendMessageBundle("X", R.id.football_home_possession);
					sendMessageBundle("", R.id.football_away_possession);
				}
				else{ //visitors have ball
					sendMessageBundle("", R.id.football_home_possession);
					sendMessageBundle("X", R.id.football_away_possession);
				}
			}
			
			private String getBallOn(String ballOnString){
				if(homeHasBall){
					if(ballOn.substring(0, 3) == homeId)
						return "-" + ballOnString.substring(3);
				}
				else{ //visitors have the ball
					if(!(ballOn.substring(0, 3) == homeId))
						return "-" + ballOnString.substring(3);
				}
				return "+" + ballOnString.substring(3);
			}
			
			private String makeTwoDigits(String number){
				while(number.length() < 2)
					number = "0" + number;
				
				return number;
			}
		};
	}
}
