/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import edu.jsu.oss.android.R;

public class ScoreboardListAdapter extends BaseAdapter {

	private Context mContext;
	private boolean[] gameComplete;
	private int[] timeUnitsElapsed;
	private Resources resources;
	//private boolean[] inSeason;

    public ScoreboardListAdapter(Context c, boolean[] gameComplete, int[] timeUnitsElapsed) {
        mContext = c;
        //this.inSeason = inSeason;
        this.gameComplete = gameComplete;
        this.timeUnitsElapsed = timeUnitsElapsed;
        resources = mContext.getResources();
    }
	
	@Override
	public int getCount() {
		return Sports.numSports;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View mView;
        
        if (convertView == null) {  // if it's not recycled, initialize some attributes
        	//set layout inflater and get the view from the xml file
        	LayoutInflater li = LayoutInflater.from(mContext);
			mView = li.inflate(R.layout.scoreboard_list_item, null);
			
			//set the sport title
			TextView sportTitle = (TextView)mView.findViewById(R.id.sport_name);
			sportTitle.setText(Sports.sportname[position]);
			
			//set the sport status
			TextView sportStatus = (TextView)mView.findViewById(R.id.sport_status);
			if (Sports.inSeason(position))
				if (gameComplete[position])
					sportStatus.setText("No games in progress");
				else
					sportStatus.setText("In progress - " + timeUnitsElapsed[position] + " " + Sports.timeUnit[position]);
			else { //sport is not in season
				sportStatus.setText("Out of season");
				sportTitle.setTextColor(resources.getColor(R.color.outOfSeason));
				sportStatus.setTextColor(resources.getColor(R.color.outOfSeason));
			}
        } else {
        	mView = convertView;
        }
        
        return mView;
	}

}
