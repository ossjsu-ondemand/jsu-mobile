/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import edu.jsu.oss.android.R;

public abstract class GamecockScoreboard extends Activity {

	protected int sport;
	
	protected String homeName;
	protected String homeId;
	protected String awayName;
	protected String awayId;

	protected GamecockUpdater scoreUpdater;
	protected PowerManager.WakeLock screenLock;
	
	protected int refreshRate;
	
	protected final int refreshConstantly = 10000;
	protected final int refreshOften = 30000;
	protected final int refreshOccasionally = 120000;
	protected final int refreshRarely = 300000;
	
	protected int contentView;
	
	private final int date = 0;
	private final int time = 1;
	private final int teamNameAway = 2;
	private final int teamIdAway = 3;
	private final int teamNameHome = 4;
	private final int teamIdHome = 5;
		
	@Override
    public synchronized void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
        setContentView(contentView);
        digitizeView();
        
        if(this.getIntent().hasExtra("sport"))
        	sport = this.getIntent().getExtras().getInt("sport");
        
        //load venue info either from the parser or from saved data
        final Object[] stateData = (Object[]) getLastNonConfigurationInstance();
    	if(stateData != null)
    		rotationQuickload(stateData);
    	else
            loadVenueInfo();
    		
        SharedPreferences prefs = getSharedPreferences("jsu_scoreboard", 0);
    	refreshRate = prefs.getInt("refreshRate", refreshOften);
    	
    	PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
    	screenLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "Scoreboard lock");
	}
	
	@Override
	public void onPause(){
		super.onPause();
		scoreUpdater.killThread();
		screenLock.release();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		
        scoreUpdater = getNewUpdater();
        scoreUpdater.start();
        
    	screenLock.acquire();
	}
	
	protected abstract GamecockUpdater getNewUpdater();
	
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.scoreboard_context, menu);
	    return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu){
	
		final int veryOften = 10000;
		final int often = 30000;
		final int seldom = 120000;
		final int verySeldom = 300000;
		
		//check the appropriate item here:
		switch (refreshRate){
			case veryOften:
				menu.findItem(R.id.constantly).setChecked(true);
				break;
			case often:
				menu.findItem(R.id.often).setChecked(true);
				break;
			case seldom:
				menu.findItem(R.id.occasionally).setChecked(true);
				break;
			case verySeldom:
				menu.findItem(R.id.rarely).setChecked(true);
				break;
		}
		return true;
	}

	@Override
	 public boolean onOptionsItemSelected(MenuItem item) {
	    
		 int newRefreshRate;
		 
		//turn the correct menu layer on/off
		switch (item.getItemId()){
		case R.id.constantly:
			newRefreshRate = refreshConstantly;
			break;
		case R.id.often:
			newRefreshRate = refreshOften;
			break;
		case R.id.occasionally:
			newRefreshRate = refreshOccasionally;
			break;
		case R.id.rarely:
			newRefreshRate = refreshRarely;
			break;
		default:
			newRefreshRate = refreshOften;
		}
		
		if(refreshRate != newRefreshRate){
			refreshRate = newRefreshRate;
			
			scoreUpdater.killThread();
			scoreUpdater =  new BaseballSoftballUpdater(scoreUpdateHandler, refreshRate, sport);
	        scoreUpdater.start();
		}
			
		return true;
	 }

	@Override
	 public Object onRetainNonConfigurationInstance(){
		return getViewData();		
	 }
	
	protected Object getViewData(){
		Object[] data = new Object[13];

   		data[date] = getViewText(R.id.scoreboard_date);
   		data[time] = getViewText(R.id.scoreboard_time);
   		data[teamNameAway] = getViewText(R.id.scoreboard_away_name);
   		data[teamNameHome] = getViewText(R.id.scoreboard_home_name);
   		data[teamIdAway] = getViewText(R.id.scoreboard_away_id);
   		data[teamIdHome] = getViewText(R.id.scoreboard_home_id);
   		
   		return data;
	}

	@Override
	public void onStop(){
		super.onStop();
		
		SharedPreferences prefs = getSharedPreferences("jsu_scoreboard", 0);
		SharedPreferences.Editor editor = prefs.edit();
		  
		editor.putInt("refreshRate", refreshRate);
		  
		editor.commit();
	}

	protected void rotationQuickload(final Object[] data){
		setTextView((String)data[date], R.id.scoreboard_date);
		setTextView((String)data[time],R.id.scoreboard_time);
		
		awayName = (String)data[teamNameAway];
		setTextView((String)data[teamNameAway], R.id.scoreboard_away_name);
		
		homeName = (String)data[teamNameHome];
		setTextView((String)data[teamNameHome], R.id.scoreboard_home_name);
		
		awayId = (String)data[teamIdAway];
		setTextView((String)data[teamIdAway], R.id.scoreboard_away_id);
		
		homeId = (String)data[teamIdHome];
		setTextView((String)data[teamIdHome], R.id.scoreboard_home_id);
	}
	
	protected void loadVenueInfo(){
		//Parse the venue data
	    SAXParser parser;
		try {
			parser = SAXParserFactory.newInstance().newSAXParser();
	        VenueHandler venueInfo = new VenueHandler();
			parser.parse(Sports.feeds[sport], venueInfo);
			
			setTextView(venueInfo.venueInfo[venueInfo.date], R.id.scoreboard_date);
			setTextView(venueInfo.venueInfo[venueInfo.time], R.id.scoreboard_time);
			
			setTextView(venueInfo.venueInfo[venueInfo.away_name], R.id.scoreboard_away_name);
			setTextView(venueInfo.venueInfo[venueInfo.home_name], R.id.scoreboard_home_name);
			setTextView(venueInfo.venueInfo[venueInfo.away_id], R.id.scoreboard_away_id);
			setTextView(venueInfo.venueInfo[venueInfo.home_id], R.id.scoreboard_home_id);
					
			
			homeName = venueInfo.venueInfo[venueInfo.home_name];
			awayName = venueInfo.venueInfo[venueInfo.away_name];
			homeId = venueInfo.venueInfo[venueInfo.home_id];
			awayId = venueInfo.venueInfo[venueInfo.away_id];
			
		} catch (Exception e) {
			//Log.e(this.getPackageName(), e.getLocalizedMessage());
		} 
	}

	protected abstract void digitizeView();

	protected void digitizeFont(TextView view){
		if(view != null)
			view.setTypeface(Typeface.createFromAsset(getAssets(), "digital.ttf"));
	}
	
	protected synchronized void setTextView(String text, int viewID){
		TextView view = (TextView) findViewById(viewID);
		if(view != null){
			view.setText(text);
			//TODO remove debug comments here
			//Log.i("GAMECOCKSCOREBOARD", "Just attempted to put " + text + " into " + view.getId());
		}
	}
	
	protected CharSequence getViewText(int viewId){
		 TextView view = (TextView)findViewById(viewId);
		 if(view != null)
			 return view.getText();
		 else
			 return "-";
	 }

	final Handler scoreUpdateHandler = new Handler(){
		public void handleMessage(Message msg){
			
			Bundle message = msg.getData();
			message.setClassLoader(getClassLoader());
			setTextView(message.getString("text"), message.getInt("viewId"));			
		}
	};
	
}
