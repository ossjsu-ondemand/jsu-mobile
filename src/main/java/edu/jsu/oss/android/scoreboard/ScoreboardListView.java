/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class ScoreboardListView extends ListActivity{

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Bundle extras = this.getIntent().getExtras();
        
        setListAdapter(new ScoreboardListAdapter(this, extras.getBooleanArray("gameComplete"), extras.getIntArray("timeUnits")));
	}

	@Override
	protected void onListItemClick (ListView l, View v, int position, long id) {
		Intent intent = new Intent();
		
		switch (position){
		case 0: //baseball
		case 1: //softball
			intent.setClass(this, BaseballSoftballScoreboard.class);
			
			Bundle extras = new Bundle();
			extras.putInt("sport", position);
			intent.putExtras(extras);
			break;
		case 2: //football
			intent.setClass(this, FootballScoreboard.class);
			break;
		case 3: //volleyball
			intent.setClass(this, VolleyballScoreboard.class);
			break;
		}
		
		startActivity(intent);
	}
}
