/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.os.Handler;
import edu.jsu.oss.android.R;

/**
 * @author Josh
 *
 */
public class BaseballSoftballUpdater extends GamecockUpdater {

	public BaseballSoftballUpdater(Handler scoreboardMessageHandler, int refreshTime, int sport) {
		super(scoreboardMessageHandler, refreshTime, sport);
		
		parserHandler = new DefaultHandler(){
			
			private boolean homeTeam;
			private String inning = "-";
			private String outs = "-";
			private String atBat;
			
			private String homeName;
			private String awayName;
			
			public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
				
				if(localName.equalsIgnoreCase("venue")){
					homeName = attributes.getValue("homename");
					awayName = attributes.getValue("visname");
				}					
				if(localName.equalsIgnoreCase("inning")){
					inning = attributes.getValue("number");
				}
				if(localName.equalsIgnoreCase("batting")){
					if (attributes.getValue("vh").equalsIgnoreCase("v"))
						atBat = awayName;
					else // home is batting
						atBat = homeName;
				}
				if(localName.equalsIgnoreCase("play")){
					outs = attributes.getValue("outs");
				}
				if(localName.equalsIgnoreCase("linescore")){
					if (homeTeam){
						sendMessageBundle(attributes.getValue("runs"), R.id.baseball_softball_runs_home);
						sendMessageBundle(attributes.getValue("hits"), R.id.baseball_softball_hits_home);
						sendMessageBundle(attributes.getValue("errs"), R.id.baseball_softball_errors_home);
					}
					else{ //awayTeam
						sendMessageBundle(attributes.getValue("runs"), R.id.baseball_softball_runs_away);
						sendMessageBundle(attributes.getValue("hits"), R.id.baseball_softball_hits_away);
						sendMessageBundle(attributes.getValue("errs"), R.id.baseball_softball_errors_away);
						
					}
				}
				if(localName.equalsIgnoreCase("team")){
					if (attributes.getValue("vh").equalsIgnoreCase("v"))
						homeTeam = false;
					else
						homeTeam = true;
				}
				if(localName.equalsIgnoreCase("lineinn")){
					if(homeTeam){
						switch (Integer.parseInt(attributes.getValue("inn"))){
						case 1:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_home_inning1);
							break;
						case 2:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_home_inning2);
							break;
						case 3:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_home_inning3);
							break;
						case 4:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_home_inning4);
							break;
						case 5:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_home_inning5);
							break;
						case 6:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_home_inning6);
							break;
						case 7:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_home_inning7);
							break;
						case 8:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_home_inning8);
							break;
						case 9:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_home_inning9);
							break;
						}
					}
					else {//it's the visiting team
						switch (Integer.parseInt(attributes.getValue("inn"))){
						case 1:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_away_inning1);
							break;
						case 2:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_away_inning2);
							break;
						case 3:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_away_inning3);
							break;
						case 4:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_away_inning4);
							break;
						case 5:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_away_inning5);
							break;
						case 6:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_away_inning6);
							break;
						case 7:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_away_inning7);
							break;
						case 8:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_away_inning8);
							break;
						case 9:
							sendMessageBundle(attributes.getValue("score"), R.id.baseball_softball_score_away_inning9);
							break;
						}
					}
				}
			}
			
			public void endDocument(){ 
				//Inning and outs are read sequentially, so when the end is reached we know that the 
				//correct inning number and outs are present, so update the views accordingly.
				sendMessageBundle(inning, R.id.baseball_softball_innings_number);
				sendMessageBundle(outs, R.id.baseball_softball_outs_number);
				sendMessageBundle(atBat, R.id.baseball_softball_at_bat);
			}
		};
	}
}
