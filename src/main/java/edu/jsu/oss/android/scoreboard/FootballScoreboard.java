/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import android.os.Bundle;
import android.widget.TextView;
import edu.jsu.oss.android.R;

public class FootballScoreboard extends GamecockScoreboard {

	private final int clock = 6;
	private final int quarter = 7;
	private final int ballOn = 8;
	private final int down = 9;
	private final int toGo = 10;
	private final int homePossession = 11;
	private final int awayPossession = 12;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        contentView = R.layout.scoreboard_football;
        sport = Sports.football;
		
		super.onCreate(savedInstanceState);
	}

	@Override
	protected GamecockUpdater getNewUpdater() {
		return new FootballUpdater(scoreUpdateHandler, refreshRate, sport);
	}

	@Override
	protected void digitizeView() {
		digitizeFont((TextView)findViewById(R.id.football_clock));
        digitizeFont((TextView)findViewById(R.id.football_home_score));
        digitizeFont((TextView)findViewById(R.id.football_quarter));
        digitizeFont((TextView)findViewById(R.id.football_away_score));
        digitizeFont((TextView)findViewById(R.id.football_ball_on));
        digitizeFont((TextView)findViewById(R.id.football_down));
        digitizeFont((TextView)findViewById(R.id.football_to_go));
        digitizeFont((TextView)findViewById(R.id.football_home_possession));
        digitizeFont((TextView)findViewById(R.id.football_away_possession));
	}
	
	protected Object getViewData(){
		Object[] data = (Object[])super.getViewData();
		
		data[clock] = getViewText(R.id.football_clock);
		data[quarter] = getViewText(R.id.football_quarter);
		data[ballOn] = getViewText(R.id.football_ball_on);
		data[down] = getViewText(R.id.football_down);
		data[toGo] = getViewText(R.id.football_to_go);
		data[homePossession] = getViewText(R.id.football_home_possession);
		data[awayPossession] = getViewText(R.id.football_away_possession);
		
		return data;
	}
	
	protected void rotationQuickload(final Object[] data){
		super.rotationQuickload(data);
		
		setTextView((String)data[clock], R.id.football_clock);
		setTextView((String)data[quarter], R.id.football_quarter);
		setTextView((String)data[ballOn], R.id.football_ball_on);
		setTextView((String)data[down], R.id.football_down);
		setTextView((String)data[toGo], R.id.football_to_go);
		setTextView((String)data[homePossession], R.id.football_home_possession);
		setTextView((String)data[awayPossession], R.id.football_away_possession);
	}
}
