/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import java.util.GregorianCalendar;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class GamesInProgress {

	public boolean[] gameComplete;
	public int[] timeUnitsElapsed;
	//public boolean[] inSeason;
	
	public GamesInProgress(){
		//inSeason = new boolean[Sports.numSports];
		gameComplete = new boolean[] {true, true, true, true}; //complete = not in progress by default
		timeUnitsElapsed = new int[Sports.numSports];
		
		//Could traverse a method array here (and invoke the appropriate function by index), but since the
		//list is small and rarely ever changes we'll go procedural for speed and readability.
		
		// Baseball
		if(Sports.inSeason(Sports.baseball)){
			//inSeason[Sports.baseball] = true;		
			getBaseballGameStatus();	
		}
		//else
			//inSeason[Sports.baseball] = false;
		
		//Softball
		if(Sports.inSeason(Sports.softball)){
			//inSeason[Sports.softball] = true;		
			getSoftballGameStatus();	
		}
		//else
			//inSeason[Sports.softball] = false;
		
		//Football
		if(Sports.inSeason(Sports.football)){
			//inSeason[Sports.football] = true;		
			getFootballGameStatus();	
		}
		//else
			//inSeason[Sports.football] = false;	
		
		if(Sports.inSeason(Sports.volleyball)){
			//inSeason[Sports.volleyball] = true;		
			getVolleyballGameStatus();	
		}
		//else
			//inSeason[Sports.volleyball] = false;	
	}
	
	private void getBaseballGameStatus(){
		try{			
			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();		
				parser.parse(Sports.feeds[Sports.baseball], new DefaultHandler() {		

					private int day;
					private int month;
					
					public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
						if(localName.equalsIgnoreCase("venue")){
							String date = attributes.getValue("date");
							int firstSlash = date.indexOf("/");
							int secondSlash = firstSlash + 1 /*to account for 0 index offset*/ + date.substring(firstSlash + 1).indexOf("/");
							month = Integer.parseInt(date.substring(0, firstSlash));
							day = Integer.parseInt(date.substring(firstSlash + 1, secondSlash));
						}
						if(localName.equalsIgnoreCase("inning"))
							timeUnitsElapsed[Sports.baseball] = Integer.parseInt(attributes.getValue("number"));
						if(localName.equalsIgnoreCase("status"))
							if(attributes.getValue("complete").equalsIgnoreCase("N"))
								if (sameDate(day, month))//if the game is posted the same day
									gameComplete[Sports.baseball] = false;				
					}
				});		
		}
		catch (Exception ex){ ex.printStackTrace();}
	}
	
	private void getSoftballGameStatus(){
		try{			
			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();		
				parser.parse(Sports.feeds[Sports.softball], new DefaultHandler() {		

					private int day;
					private int month;
					
					public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
						if(localName.equalsIgnoreCase("venue")){
							String date = attributes.getValue("date");
							int firstSlash = date.indexOf("/");
							int secondSlash = firstSlash + 1 /*to account for 0 index offset*/ + date.substring(firstSlash + 1).indexOf("/");
							month = Integer.parseInt(date.substring(0, firstSlash));
							day = Integer.parseInt(date.substring(firstSlash + 1, secondSlash));
						}
						if(localName.equalsIgnoreCase("inning"))
							timeUnitsElapsed[Sports.softball] = Integer.parseInt(attributes.getValue("number"));
						if(localName.equalsIgnoreCase("status")) //if the game is in progress change the flag (otherwise it is initialized to true)
							if(attributes.getValue("complete").equalsIgnoreCase("N"))
								if (sameDate(day, month))//if the game is posted the same day
									gameComplete[Sports.softball] = false;						
					}
				});		
		}
		catch (Exception ex){ ex.printStackTrace();}
	}
	
	private void getFootballGameStatus(){
		try{			
			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();		
				parser.parse(Sports.feeds[Sports.football], new DefaultHandler() {		
					
					public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
						if(localName.equalsIgnoreCase("qtr"))
							timeUnitsElapsed[Sports.football] = Integer.parseInt(attributes.getValue("number"));
						if(localName.equalsIgnoreCase("status")) //if the game is in progress change the flag (otherwise it is initialized to true)
							// TODO ensure football posts complete here.
							if(attributes.getValue("complete").equalsIgnoreCase("N"))
								gameComplete[Sports.football] = false;				
					}
				});		
		}
		catch (Exception ex){ ex.printStackTrace();}//TODO do something with exception
	}
	
	private Boolean sameDate(int day, int month){
		boolean sameDate = false;
		GregorianCalendar calendar = new GregorianCalendar();
		
		if(calendar.get(GregorianCalendar.DAY_OF_MONTH) == day)
			if(calendar.get(GregorianCalendar.MONTH) == month)
				sameDate = true;
		
		return sameDate;
	}
	
	private void getVolleyballGameStatus(){
		// TODO implement volleyball game status method
	}
}
