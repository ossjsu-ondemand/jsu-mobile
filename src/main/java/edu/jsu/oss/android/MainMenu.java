/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android;


public class MainMenu {
	
	//Define all menu items - this is the fixed list all others will access
	private static MenuItem[] menu_items = 
		{new MenuItem(R.drawable.icon_nav, R.string.map_title),
		new MenuItem(R.drawable.icon_news, R.string.chanticleer_title),
		new MenuItem(R.drawable.icon_scoreboard, R.string.scoreboard_title),
		new MenuItem(R.drawable.icon_rss, R.string.news_title)};
	
	//gets the reference for the icon resource
	public static int getIcon(int index){
		return menu_items[index].getIcon();
	}
	
	//gets the reference for the text resource
	public static int getText(int index){
		return menu_items[index].getText();
	}
	
	public static int getLength(){
		return menu_items.length;
	}
}
