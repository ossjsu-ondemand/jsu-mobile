/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import edu.jsu.oss.android.R;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.MapController;
import com.google.android.maps.Projection;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.Overlay;

public class CampusMap extends MapActivity{
	
	private Intent intent;
	
	private MapView mapView;
	private MapController mapController;
	private Location location;
	private List<Overlay> overlays;
	private LocationManager locationManager;
	private ProgressDialog waitDialog;
	
	private int searchZoom;
	private boolean searchFocus;
	
	private boolean showParking;
	private boolean showBusRoutes;
	
	private boolean firstLocation;
	private boolean goneForGPSsettings;
	
	private BuildingItemizedOverlay buildingIcons;
	private BuildingPolygonOverlay buildingPolys;
	private ParkingPolygonOverlay parking;
	private BusRouteOverlay busRoutes;
	
	private final int gpsEnabled = 0;
	private final int zoomLevel = 1;
	private final int centerPoint = 2;
	private final int firstLoc = 3;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    setContentView(R.layout.campus_map);
	        
    	initializeComponents();
    	
    	searchFocus = false;
    	goneForGPSsettings = false;

    	// Determine which layers are to be painted
    	SharedPreferences prefs = getSharedPreferences("jsu_campus_map", 0);
    	showParking = prefs.getBoolean("parking", false);
    	showBusRoutes = prefs.getBoolean("bus", false);
    	
    	//initialize and add overlays
    	addOverlays();
    	
       //Should occur only when map is launched, not on search or return from search/orientation change
    	final Object[] stateData = (Object[]) getLastNonConfigurationInstance();
    	if(stateData != null){
			firstLocation = (Boolean)stateData[firstLoc];
    		
			if ((Boolean)stateData[gpsEnabled]){
				LocationAcquired(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
    		}
    		else{
    			mapController.setCenter((GeoPoint)stateData[centerPoint]);
    			mapController.setZoom(((Number)stateData[zoomLevel]).intValue());
    		}
    	}
    	else{//it's a fresh instance
    		firstLocation = true;
            mapController.setCenter(new GeoPoint(33822547, -85765993));
		    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
		          showGPSDisabledAlertToUser();
		          //We set the center here in case of GPS remains off so that the map is centered on campus
		    }else {//GPS is enabled, so give a wait dialog
		    	showAcquiringGPS();
			    
		    }
    	}
	}
	
	private void showAcquiringGPS(){
		waitDialog = new ProgressDialog(this);
	    waitDialog.setTitle("Determining Location");
	    waitDialog.setMessage("Please wait...");
	    waitDialog.setCancelable(false); //keeps users from pressing the back key and avoiding the listener
	    waitDialog.setButton("Cancel", new DialogInterface.OnClickListener() 
	    {
	        public void onClick(DialogInterface dialog, int which) 
	        {
	            waitDialog.dismiss();
	            locationManager.removeUpdates(locationListener);
	        }
	    });
	    waitDialog.show();
	    
    	locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 5, locationListener);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if(goneForGPSsettings){
			showAcquiringGPS();			
			goneForGPSsettings = false;
		}
	}
	
	@Override
	public void onNewIntent(Intent intent) {
		this.intent = intent; //this may not be necessary - just pass it to function
		handleIntent();
	}
	
	private void handleIntent(){
		if (Intent.ACTION_VIEW.equals(intent.getAction())) {
			searchFocus = false;
            // handles a click on a search suggestion; launches activity to show word
        	int lat, lon;
            Uri uri = intent.getData();
            Cursor c = getContentResolver().query(uri, null, null, null, null);
            try {
                c.moveToFirst();
                lat = Integer.parseInt(c.getString(2));
                lon = Integer.parseInt(c.getString(6));
            } finally {
                c.close();
            }
                 
            buildingIcons.onTap(new GeoPoint(lat, lon), mapView);
        }
	}
	
	@Override 
	public boolean onSearchRequested(){
		searchZoom = mapView.getZoomLevel();
		searchFocus = true;
		mapController.setZoom(19);
		
		return super.onSearchRequested();
	}
	
	//called on view swtich
	@Override
	public Object onRetainNonConfigurationInstance(){
		//determine if GPS is enabled
		Object[] data = new Object[4];
		data[gpsEnabled] = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		data[zoomLevel] = mapView.getZoomLevel(); 
		data[centerPoint] = mapView.getMapCenter();
		data[firstLoc] = firstLocation;
		return data;
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus){
		if(searchFocus)
			if(hasFocus){
				mapController.setZoom(searchZoom);
				searchFocus = false;
			}
		
		super.onWindowFocusChanged(hasFocus);
	}
	
	private void initializeComponents(){		
		locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
		
		//set mapView properties
		mapView = (MapView) findViewById(R.id.campusMapview);
	    mapView.setBuiltInZoomControls(true);
	    
	    //Get the controller and customize
	    mapController = mapView.getController();
	   	mapController.setZoom(18);
	   	
		overlays = mapView.getOverlays();
	}
	
	private void addOverlays(){
		
		buildingIcons = new BuildingItemizedOverlay(this, getResources().getDrawable(R.drawable.map_placemark), mapView.getZoomLevel(), mapView);
		overlays.add(buildingIcons);
		buildingPolys = new BuildingPolygonOverlay(true, false);
		overlays.add(buildingPolys);
	
		if (showParking){
			parking = new ParkingPolygonOverlay();
			overlays.add(parking);
		}
		
		if (showBusRoutes){
			busRoutes = new BusRouteOverlay();
			overlays.add(busRoutes);
		}
	}
	
	private void LocationAcquired(Location location){
		this.location = location;
		
		//Determine if the location is on campus (if it's the first location fix)
		
		if(!onCampus()){
			//This should only be shown if it's the first location fix.  Otherwise, the user may wish to navigate
			//without using GPS (although it is enabled) so the app should not continue to ask. 
			if(firstLocation){
				showNavigationAlertToUser();
				firstLocation = false;
			}
		}
		else{
			//center the map on the location
			mapController.setCenter(new GeoPoint((int)(location.getLatitude() * 1E6),(int)(location.getLongitude() * 1E6)));
			
			//Add a location overlay to indicate position
			MyLocationOverlay locationOverlay = new MyLocationOverlay(this, mapView);
			overlays.add(locationOverlay);
			locationOverlay.enableMyLocation();
		}
		//Location listener will be turned off automatically by the feedType once the first point
		//is created because we only need 1 point to determine campus location.
	}
	
	private void showGPSDisabledAlertToUser(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
			alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to go to settings to enable it?")
		     .setCancelable(false)
		     .setPositiveButton("Yes",
		          new DialogInterface.OnClickListener(){
		          public void onClick(DialogInterface dialog, int id){
		        	  goneForGPSsettings = true;
		        	  
		        	  Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		        	  startActivity(callGPSSettingIntent);
		          }
		     });
		     alertDialogBuilder.setNegativeButton("No",
		          new DialogInterface.OnClickListener(){
		          public void onClick(DialogInterface dialog, int id){
		               dialog.cancel();
		          }
		     });
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}
	
	private boolean onCampus(){
		if(location.getLatitude() > 33.818519 && location.getLatitude() < 33.830569)
			if(location.getLongitude() > -85.777431 && location.getLongitude() < -85.759792)
				return true;
		//Else
		return false;
	}
	
	private void showNavigationAlertToUser(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
			alertDialogBuilder.setMessage("You are not currently on campus, would you like to navigate there?")
		     .setCancelable(false)
		     .setPositiveButton("Yes",
		          new DialogInterface.OnClickListener(){
		          public void onClick(DialogInterface dialog, int id){
		        	  Intent NavigateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=700+Pelham+Rd+N+Jacksonville+AL+36265"));
		  			startActivity(NavigateIntent);
		          }
		     });
		     alertDialogBuilder.setNegativeButton("No",
		          new DialogInterface.OnClickListener(){
		          public void onClick(DialogInterface dialog, int id){
		               dialog.cancel();
		          }
		     });
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}
	
	//This listener will only be used to check the original location, as 
	//the mylocationoverlay updates itself.
	  private final LocationListener locationListener = new LocationListener() {
		    public void onLocationChanged(Location location) {
		    	//This event will only occur once, as the listener is turned off at the end.
				waitDialog.dismiss();
				LocationAcquired(location);
		  
		        //turn the updates off (since myLocationOverlay will deal w/ location updates)
		        //therefore, we only need an initial starting point
				locationManager.removeUpdates(locationListener);
		    }			 
		    public void onProviderDisabled(String provider){
		    	//Be sure NOT to remove updates here, as they are required to determine when or if
		    	//the provider is ever re-enabled
		    }
		    public void onProviderEnabled(String provider){
			    //If the GPS is turned on (from outside the application), requests need to be made
			    //to ensure the location arrives.
		    	locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		    }
		    public void onStatusChanged(String provider, int status, Bundle extras){ }
		  };

	  //called only the 1st time the menu is built (onCreate), specifies the layout
	  public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.map_context, menu);
	    return true;
	  }
	  
	  //called each successive time the menu is loaded
	  @Override
	  public boolean onPrepareOptionsMenu(Menu menu){

		//check the appropriate items here:
		if (showParking)
			menu.findItem(R.id.parking).setChecked(true);
		else
			menu.findItem(R.id.parking).setChecked(false);
		
		if (showBusRoutes)
			menu.findItem(R.id.bus).setChecked(true);
		else
			menu.findItem(R.id.bus).setChecked(false);
			
	    return true;
	  }
	  
	  @Override
	  public boolean onOptionsItemSelected(MenuItem item) {
	    
		//turn the correct menu layer on/off
		switch (item.getItemId()){
		case R.id.map_search:
			this.onSearchRequested();
			break;
		case R.id.parking:
			if(item.isChecked()){
				showParking = false;
				overlays.remove(parking);
			}
			else{
				showParking = true;
				if(parking != null)
					overlays.add(parking);
				else{
					parking = new ParkingPolygonOverlay();
					overlays.add(parking);
				}
			}
			break;
		case R.id.bus:
			if(item.isChecked()){
				showBusRoutes = false;
				overlays.remove(busRoutes);
			}
			else{
				showBusRoutes = true;
				BusRoutesInProgress routes = new BusRoutesInProgress();
				if(busRoutes != null){
					overlays.add(busRoutes);
					if(!routes.hasRoutes)
						Toast.makeText(this, "No bus routes in progress", Toast.LENGTH_LONG).show();				
				}	
				else{
					busRoutes = new BusRouteOverlay();
					overlays.add(busRoutes);
					if(!routes.hasRoutes)
						Toast.makeText(this, "No bus routes in progress", Toast.LENGTH_LONG).show();
				}
			}
			break;
		}

		mapView.invalidate();
	    return true;
	  }

	   
	  @Override
	  protected void onStop(){
		  super.onStop();
		  
		  SharedPreferences prefs = getSharedPreferences("jsu_campus_map", 0);
		  SharedPreferences.Editor editor = prefs.edit();
		  
		  editor.putBoolean("parking", showParking);
		  editor.putBoolean("bus", showBusRoutes);
		  
		  editor.commit();
	  }		
		
	  @Override
	  protected boolean isRouteDisplayed() {
		  return false;
	  }
}