/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.util.ArrayList;

import android.graphics.Color;

import com.google.android.maps.GeoPoint;

public final class BusRouteData {

	public static ArrayList<GeoPoint> getRedLine(){
		ArrayList<GeoPoint> line = new ArrayList<GeoPoint>();

		line.add(new GeoPoint(33821331, -85766350));
		line.add(new GeoPoint(33821007, -85766289));
		line.add(new GeoPoint(33821072, -85765701));
		line.add(new GeoPoint(33821136, -85765419));
		line.add(new GeoPoint(33821388, -85765022));
		line.add(new GeoPoint(33821602, -85764755));
		line.add(new GeoPoint(33821823, -85764595));
		line.add(new GeoPoint(33822025, -85764511));
		line.add(new GeoPoint(33822227, -85764511));
		line.add(new GeoPoint(33822479, -85764534));
		line.add(new GeoPoint(33822620, -85764519));
		line.add(new GeoPoint(33822754, -85764473));
		line.add(new GeoPoint(33822849, -85764381));
		line.add(new GeoPoint(33822857, -85764038));
		line.add(new GeoPoint(33822876, -85763985));
		line.add(new GeoPoint(33822971, -85764076));
		line.add(new GeoPoint(33823051, -85764267));
		line.add(new GeoPoint(33823151, -85764435));
		line.add(new GeoPoint(33823311, -85764618));
		line.add(new GeoPoint(33823441, -85764709));
		line.add(new GeoPoint(33823574, -85764732));
		line.add(new GeoPoint(33823711, -85764702));
		line.add(new GeoPoint(33823860, -85764656));
		line.add(new GeoPoint(33823849, -85764946));
		line.add(new GeoPoint(33823833, -85765251));
		line.add(new GeoPoint(33823803, -85765640));
		line.add(new GeoPoint(33823772, -85765984));
		line.add(new GeoPoint(33823746, -85766388));
		line.add(new GeoPoint(33823673, -85766685));
		line.add(new GeoPoint(33823586, -85767052));
		line.add(new GeoPoint(33823502, -85767349));
		line.add(new GeoPoint(33823406, -85767624));
		line.add(new GeoPoint(33823311, -85767784));
		line.add(new GeoPoint(33823135, -85767952));
		line.add(new GeoPoint(33822914, -85768051));
		line.add(new GeoPoint(33822746, -85768097));
		line.add(new GeoPoint(33822525, -85768089));
		line.add(new GeoPoint(33822334, -85768051));
		line.add(new GeoPoint(33821960, -85767868));
		line.add(new GeoPoint(33821617, -85767647));
		line.add(new GeoPoint(33821419, -85767509));
		line.add(new GeoPoint(33821205, -85767326));
		line.add(new GeoPoint(33821091, -85767525));
		line.add(new GeoPoint(33821033, -85767632));
		line.add(new GeoPoint(33821030, -85767746));
		line.add(new GeoPoint(33821045, -85767868));
		line.add(new GeoPoint(33821041, -85767952));
		line.add(new GeoPoint(33820900, -85768250));
		line.add(new GeoPoint(33820843, -85768402));
		line.add(new GeoPoint(33820820, -85768539));
		line.add(new GeoPoint(33820827, -85768768));
		line.add(new GeoPoint(33821922, -85768730));
		line.add(new GeoPoint(33822708, -85768730));
		line.add(new GeoPoint(33823715, -85768715));
		line.add(new GeoPoint(33824966, -85768677));
		line.add(new GeoPoint(33825890, -85768654));
		line.add(new GeoPoint(33825882, -85766937));
		line.add(new GeoPoint(33825855, -85765648));
		line.add(new GeoPoint(33825851, -85764854));
		line.add(new GeoPoint(33827023, -85764824));
		line.add(new GeoPoint(33827007, -85763962));
		line.add(new GeoPoint(33828117, -85763916));
		line.add(new GeoPoint(33828964, -85763885));
		line.add(new GeoPoint(33829514, -85763878));
		line.add(new GeoPoint(33829445, -85763626));
		line.add(new GeoPoint(33829395, -85762718));
		line.add(new GeoPoint(33829777, -85761642));
		line.add(new GeoPoint(33829979, -85761726));
		line.add(new GeoPoint(33830650, -85761749));
		line.add(new GeoPoint(33830597, -85759766));
		line.add(new GeoPoint(33830242, -85759789));
		line.add(new GeoPoint(33829540, -85759796));
		line.add(new GeoPoint(33829212, -85759804));
		line.add(new GeoPoint(33829056, -85759766));
		line.add(new GeoPoint(33828690, -85759766));
		line.add(new GeoPoint(33827560, -85759743));
		line.add(new GeoPoint(33826069, -85759727));
		line.add(new GeoPoint(33826012, -85760063));
		line.add(new GeoPoint(33825825, -85760590));
		line.add(new GeoPoint(33825806, -85760727));
		line.add(new GeoPoint(33825817, -85761749));
		line.add(new GeoPoint(33825825, -85762268));
		line.add(new GeoPoint(33825066, -85762482));
		line.add(new GeoPoint(33824951, -85763123));
		line.add(new GeoPoint(33824722, -85763077));
		line.add(new GeoPoint(33824818, -85762497));
		line.add(new GeoPoint(33824486, -85762459));
		line.add(new GeoPoint(33824490, -85761612));
		line.add(new GeoPoint(33825588, -85761246));
		line.add(new GeoPoint(33825825, -85761238));
		line.add(new GeoPoint(33825813, -85760666));
		line.add(new GeoPoint(33825966, -85760201));
		line.add(new GeoPoint(33826069, -85759766));
		line.add(new GeoPoint(33826050, -85759735));
		line.add(new GeoPoint(33824898, -85759743));
		line.add(new GeoPoint(33823898, -85759758));
		line.add(new GeoPoint(33823421, -85761292));
		line.add(new GeoPoint(33823120, -85762276));
		line.add(new GeoPoint(33823097, -85762474));
		line.add(new GeoPoint(33823116, -85762726));
		line.add(new GeoPoint(33823120, -85762917));
		line.add(new GeoPoint(33823009, -85763138));
		line.add(new GeoPoint(33824329, -85763718));
		line.add(new GeoPoint(33824253, -85764053));
		line.add(new GeoPoint(33824150, -85764366));
		line.add(new GeoPoint(33824097, -85764488));
		line.add(new GeoPoint(33824001, -85764618));
		line.add(new GeoPoint(33823860, -85764656));
		
		return line;
	}
	
	public static ArrayList<GeoPoint> getWhiteLine(){
		ArrayList<GeoPoint> line = new ArrayList<GeoPoint>();

		line.add(new GeoPoint(33821320, -85766342));
		line.add(new GeoPoint(33820980, -85766289));
		line.add(new GeoPoint(33821129, -85765411));
		line.add(new GeoPoint(33821232, -85765244));
		line.add(new GeoPoint(33821579, -85764771));
		line.add(new GeoPoint(33821819, -85764580));
		line.add(new GeoPoint(33822060, -85764503));
		line.add(new GeoPoint(33822540, -85764542));
		line.add(new GeoPoint(33822681, -85764511));
		line.add(new GeoPoint(33822842, -85764389));
		line.add(new GeoPoint(33822861, -85763977));
		line.add(new GeoPoint(33822708, -85763992));
		line.add(new GeoPoint(33822441, -85763901));
		line.add(new GeoPoint(33821758, -85763878));
		line.add(new GeoPoint(33821270, -85763702));
		line.add(new GeoPoint(33820995, -85763611));
		line.add(new GeoPoint(33820778, -85763451));
		line.add(new GeoPoint(33820950, -85762199));
		line.add(new GeoPoint(33819229, -85761459));
		line.add(new GeoPoint(33818531, -85761269));
		line.add(new GeoPoint(33818501, -85764877));
		line.add(new GeoPoint(33819012, -85765045));
		line.add(new GeoPoint(33819267, -85766159));
		line.add(new GeoPoint(33819050, -85767357));
		line.add(new GeoPoint(33818508, -85767509));
		line.add(new GeoPoint(33818531, -85768631));
		line.add(new GeoPoint(33819931, -85768791));
		line.add(new GeoPoint(33825901, -85768661));
		line.add(new GeoPoint(33825939, -85775108));
		line.add(new GeoPoint(33825748, -85777382));
		line.add(new GeoPoint(33822220, -85777397));
		line.add(new GeoPoint(33822239, -85772507));
		line.add(new GeoPoint(33824619, -85772507));
		line.add(new GeoPoint(33824619, -85774384));
		line.add(new GeoPoint(33823631, -85774399));
		line.add(new GeoPoint(33823589, -85771988));
		line.add(new GeoPoint(33823391, -85770851));
		line.add(new GeoPoint(33823391, -85768723));
		line.add(new GeoPoint(33825901, -85768661));
		line.add(new GeoPoint(33825878, -85766617));
		line.add(new GeoPoint(33824509, -85766678));
		line.add(new GeoPoint(33824291, -85766762));
		line.add(new GeoPoint(33823738, -85766373));
		line.add(new GeoPoint(33823582, -85767021));
		line.add(new GeoPoint(33823330, -85767738));
		line.add(new GeoPoint(33823120, -85767960));
		line.add(new GeoPoint(33822689, -85768097));
		line.add(new GeoPoint(33822418, -85768066));
		line.add(new GeoPoint(33822021, -85767899));
		line.add(new GeoPoint(33821209, -85767326));
		line.add(new GeoPoint(33820961, -85766670));
		line.add(new GeoPoint(33820980, -85766289));
		
		return line;
	}
	
	public static ArrayList<GeoPoint> getPurpleLine(){
		ArrayList<GeoPoint> line = new ArrayList<GeoPoint>();
		
		line.add(new GeoPoint(33821339, -85766342));
		line.add(new GeoPoint(33820992, -85766281));
		line.add(new GeoPoint(33821121, -85765533));
		line.add(new GeoPoint(33821362, -85765068));
		line.add(new GeoPoint(33821579, -85764771));
		line.add(new GeoPoint(33821972, -85764526));
		line.add(new GeoPoint(33822670, -85764519));
		line.add(new GeoPoint(33822849, -85764381));
		line.add(new GeoPoint(33822861, -85763969));
		line.add(new GeoPoint(33822720, -85764000));
		line.add(new GeoPoint(33822300, -85763893));
		line.add(new GeoPoint(33821831, -85763893));
		line.add(new GeoPoint(33821224, -85763680));
		line.add(new GeoPoint(33820999, -85763603));
		line.add(new GeoPoint(33820774, -85763451));
		line.add(new GeoPoint(33820942, -85762199));
		line.add(new GeoPoint(33823002, -85763130));
		line.add(new GeoPoint(33823120, -85762917));
		line.add(new GeoPoint(33823101, -85762451));
		line.add(new GeoPoint(33823132, -85762238));
		line.add(new GeoPoint(33823891, -85759758));
		line.add(new GeoPoint(33826069, -85759727));
		line.add(new GeoPoint(33826019, -85760033));
		line.add(new GeoPoint(33825821, -85760612));
		line.add(new GeoPoint(33825821, -85762268));
		line.add(new GeoPoint(33825062, -85762459));
		line.add(new GeoPoint(33824951, -85763123));
		line.add(new GeoPoint(33824711, -85763069));
		line.add(new GeoPoint(33824810, -85762489));
		line.add(new GeoPoint(33824471, -85762459));
		line.add(new GeoPoint(33824478, -85761597));
		line.add(new GeoPoint(33825569, -85761230));
		line.add(new GeoPoint(33825829, -85761238));
		line.add(new GeoPoint(33825821, -85760582));
		line.add(new GeoPoint(33826080, -85759720));
		line.add(new GeoPoint(33829010, -85759773));
		line.add(new GeoPoint(33829239, -85759811));
		line.add(new GeoPoint(33830601, -85759743));
		line.add(new GeoPoint(33830650, -85761749));
		line.add(new GeoPoint(33829948, -85761726));
		line.add(new GeoPoint(33829762, -85761642));
		line.add(new GeoPoint(33829391, -85762672));
		line.add(new GeoPoint(33829491, -85763847));
		line.add(new GeoPoint(33828251, -85763908));
		line.add(new GeoPoint(33828270, -85764778));
		line.add(new GeoPoint(33827030, -85764832));
		line.add(new GeoPoint(33826988, -85763962));
		line.add(new GeoPoint(33826302, -85764023));
		line.add(new GeoPoint(33825829, -85764000));
		line.add(new GeoPoint(33825890, -85768661));
		line.add(new GeoPoint(33824440, -85768677));
		line.add(new GeoPoint(33824409, -85767113));
		line.add(new GeoPoint(33824360, -85766899));
		line.add(new GeoPoint(33824280, -85766739));
		line.add(new GeoPoint(33824070, -85766640));
		line.add(new GeoPoint(33823738, -85766380));
		line.add(new GeoPoint(33823391, -85767609));
		line.add(new GeoPoint(33823120, -85767937));
		line.add(new GeoPoint(33822762, -85768089));
		line.add(new GeoPoint(33822380, -85768066));
		line.add(new GeoPoint(33822151, -85767967));
		line.add(new GeoPoint(33821678, -85767700));
		line.add(new GeoPoint(33821190, -85767319));
		line.add(new GeoPoint(33821011, -85766891));
		line.add(new GeoPoint(33820961, -85766663));
		line.add(new GeoPoint(33820992, -85766281));
		line.add(new GeoPoint(33821339, -85766342));
		
		return line;
	}
	
	public static ArrayList<GeoPoint> getBlueLine(){
		ArrayList<GeoPoint> line = new ArrayList<GeoPoint>();

		line.add(new GeoPoint(33821320, -85766327));
		line.add(new GeoPoint(33820980, -85766289));
		line.add(new GeoPoint(33821049, -85765770));
		line.add(new GeoPoint(33821171, -85765381));
		line.add(new GeoPoint(33821381, -85765030));
		line.add(new GeoPoint(33821659, -85764687));
		line.add(new GeoPoint(33821960, -85764519));
		line.add(new GeoPoint(33822601, -85764526));
		line.add(new GeoPoint(33822842, -85764381));
		line.add(new GeoPoint(33822861, -85763809));
		line.add(new GeoPoint(33823002, -85763123));
		line.add(new GeoPoint(33820438, -85761948));
		line.add(new GeoPoint(33819431, -85761528));
		line.add(new GeoPoint(33818729, -85761307));
		line.add(new GeoPoint(33818378, -85761261));
		line.add(new GeoPoint(33815331, -85761253));
		line.add(new GeoPoint(33815338, -85764000));
		line.add(new GeoPoint(33814892, -85765099));
		line.add(new GeoPoint(33815338, -85764000));
		line.add(new GeoPoint(33815300, -85761467));
		line.add(new GeoPoint(33815331, -85761253));
		line.add(new GeoPoint(33814381, -85761269));
		line.add(new GeoPoint(33813961, -85761581));
		line.add(new GeoPoint(33813740, -85761597));
		line.add(new GeoPoint(33813568, -85761551));
		line.add(new GeoPoint(33813278, -85761299));
		line.add(new GeoPoint(33807861, -85761360));
		line.add(new GeoPoint(33805241, -85761314));
		line.add(new GeoPoint(33805241, -85762520));
		line.add(new GeoPoint(33803940, -85762489));
		line.add(new GeoPoint(33802700, -85762482));
		line.add(new GeoPoint(33802681, -85761299));
		line.add(new GeoPoint(33797939, -85761261));
		line.add(new GeoPoint(33797741, -85761337));
		line.add(new GeoPoint(33795071, -85761330));
		line.add(new GeoPoint(33789280, -85761208));
		line.add(new GeoPoint(33789295, -85762619));
		line.add(new GeoPoint(33788395, -85762627));
		line.add(new GeoPoint(33789295, -85762642));
		line.add(new GeoPoint(33789291, -85760971));
		line.add(new GeoPoint(33797729, -85761200));
		line.add(new GeoPoint(33797939, -85761261));
		line.add(new GeoPoint(33801151, -85761307));
		line.add(new GeoPoint(33800999, -85760002));
		line.add(new GeoPoint(33805012, -85759872));
		line.add(new GeoPoint(33805550, -85755493));
		line.add(new GeoPoint(33806000, -85754288));
		line.add(new GeoPoint(33806358, -85753708));
		line.add(new GeoPoint(33807281, -85753738));
		line.add(new GeoPoint(33807571, -85753700));
		line.add(new GeoPoint(33807751, -85753502));
		line.add(new GeoPoint(33808121, -85753441));
		line.add(new GeoPoint(33809380, -85753464));
		line.add(new GeoPoint(33808121, -85753441));
		line.add(new GeoPoint(33807751, -85753502));
		line.add(new GeoPoint(33807571, -85753700));
		line.add(new GeoPoint(33807281, -85753738));
		line.add(new GeoPoint(33806358, -85753708));
		line.add(new GeoPoint(33806000, -85754288));
		line.add(new GeoPoint(33805550, -85755493));
		line.add(new GeoPoint(33805012, -85759872));
		line.add(new GeoPoint(33807159, -85759872));
		line.add(new GeoPoint(33807159, -85760872));
		line.add(new GeoPoint(33807159, -85759872));
		line.add(new GeoPoint(33816330, -85759789));
		line.add(new GeoPoint(33816319, -85761253));
		line.add(new GeoPoint(33818378, -85761261));
		line.add(new GeoPoint(33819431, -85761528));
		line.add(new GeoPoint(33824322, -85763710));
		line.add(new GeoPoint(33824200, -85764214));
		line.add(new GeoPoint(33824081, -85764511));
		line.add(new GeoPoint(33824001, -85764618));
		line.add(new GeoPoint(33823849, -85764664));
		line.add(new GeoPoint(33823738, -85766388));
		line.add(new GeoPoint(33823391, -85767609));
		line.add(new GeoPoint(33823109, -85767967));
		line.add(new GeoPoint(33822762, -85768082));
		line.add(new GeoPoint(33822449, -85768066));
		line.add(new GeoPoint(33822071, -85767929));
		line.add(new GeoPoint(33821201, -85767342));
		line.add(new GeoPoint(33820950, -85766609));
		line.add(new GeoPoint(33820980, -85766289));
		
		return line;
	}
	
	public static ArrayList<GeoPoint> getBlackLine(){
		ArrayList<GeoPoint> line = new ArrayList<GeoPoint>();

		line.add(new GeoPoint(33821320, -85766342));
		line.add(new GeoPoint(33820992, -85766281));
		line.add(new GeoPoint(33821140, -85765411));
		line.add(new GeoPoint(33821308, -85765121));
		line.add(new GeoPoint(33821640, -85764717));
		line.add(new GeoPoint(33821918, -85764542));
		line.add(new GeoPoint(33822151, -85764488));
		line.add(new GeoPoint(33822609, -85764519));
		line.add(new GeoPoint(33822830, -85764397));
		line.add(new GeoPoint(33822861, -85763748));
		line.add(new GeoPoint(33823002, -85763130));
		line.add(new GeoPoint(33819172, -85761421));
		line.add(new GeoPoint(33818520, -85761269));
		line.add(new GeoPoint(33816109, -85761238));
		line.add(new GeoPoint(33814369, -85761269));
		line.add(new GeoPoint(33814190, -85761421));
		line.add(new GeoPoint(33814209, -85762177));
		line.add(new GeoPoint(33813412, -85762192));
		line.add(new GeoPoint(33813450, -85761414));
		line.add(new GeoPoint(33813290, -85761307));
		line.add(new GeoPoint(33808208, -85761353));
		line.add(new GeoPoint(33803940, -85761292));
		line.add(new GeoPoint(33803951, -85762482));
		line.add(new GeoPoint(33802689, -85762482));
		line.add(new GeoPoint(33802681, -85761276));
		line.add(new GeoPoint(33797920, -85761261));
		line.add(new GeoPoint(33797729, -85761353));
		line.add(new GeoPoint(33789272, -85761208));
		line.add(new GeoPoint(33789310, -85762634));
		line.add(new GeoPoint(33788399, -85762627));
		line.add(new GeoPoint(33789295, -85762634));
		line.add(new GeoPoint(33789280, -85760971));
		line.add(new GeoPoint(33797722, -85761200));
		line.add(new GeoPoint(33797989, -85761269));
		line.add(new GeoPoint(33801140, -85761299));
		line.add(new GeoPoint(33801010, -85760010));
		line.add(new GeoPoint(33804790, -85759880));
		line.add(new GeoPoint(33807159, -85759880));
		line.add(new GeoPoint(33807171, -85760872));
		line.add(new GeoPoint(33807159, -85759880));
		line.add(new GeoPoint(33816319, -85759789));
		line.add(new GeoPoint(33816330, -85761253));
		line.add(new GeoPoint(33818520, -85761269));
		line.add(new GeoPoint(33819172, -85761421));
		line.add(new GeoPoint(33824329, -85763710));
		line.add(new GeoPoint(33824108, -85764458));
		line.add(new GeoPoint(33823841, -85764671));
		line.add(new GeoPoint(33823730, -85766388));
		line.add(new GeoPoint(33823391, -85767632));
		line.add(new GeoPoint(33823200, -85767868));
		line.add(new GeoPoint(33822922, -85768051));
		line.add(new GeoPoint(33822731, -85768097));
		line.add(new GeoPoint(33822418, -85768082));
		line.add(new GeoPoint(33821701, -85767693));
		line.add(new GeoPoint(33821190, -85767319));
		line.add(new GeoPoint(33821011, -85766891));
		line.add(new GeoPoint(33820950, -85766632));
		line.add(new GeoPoint(33820992, -85766281));
		
		return line;
	}
	
	public static ArrayList<GeoPoint> getYellowLine(){
		ArrayList<GeoPoint> line = new ArrayList<GeoPoint>();

		line.add(new GeoPoint(33821320, -85766327));
		line.add(new GeoPoint(33820992, -85766289));
		line.add(new GeoPoint(33821178, -85765297));
		line.add(new GeoPoint(33821602, -85764748));
		line.add(new GeoPoint(33821812, -85764580));
		line.add(new GeoPoint(33822048, -85764503));
		line.add(new GeoPoint(33822632, -85764526));
		line.add(new GeoPoint(33822849, -85764374));
		line.add(new GeoPoint(33822849, -85763832));
		line.add(new GeoPoint(33823002, -85763130));
		line.add(new GeoPoint(33819550, -85761581));
		line.add(new GeoPoint(33818878, -85761337));
		line.add(new GeoPoint(33818531, -85761269));
		line.add(new GeoPoint(33814381, -85761269));
		line.add(new GeoPoint(33814201, -85761429));
		line.add(new GeoPoint(33814220, -85762192));
		line.add(new GeoPoint(33813431, -85762192));
		line.add(new GeoPoint(33813450, -85761421));
		line.add(new GeoPoint(33813301, -85761307));
		line.add(new GeoPoint(33812881, -85761307));
		line.add(new GeoPoint(33812881, -85760361));
		line.add(new GeoPoint(33812347, -85760384));
		line.add(new GeoPoint(33812363, -85761307));
		line.add(new GeoPoint(33812881, -85761314));
		line.add(new GeoPoint(33812878, -85760361));
		line.add(new GeoPoint(33813438, -85760338));
		line.add(new GeoPoint(33813419, -85759811));
		line.add(new GeoPoint(33816330, -85759781));
		line.add(new GeoPoint(33816330, -85761253));
		line.add(new GeoPoint(33818531, -85761269));
		line.add(new GeoPoint(33819031, -85761391));
		line.add(new GeoPoint(33820412, -85761948));
		line.add(new GeoPoint(33823311, -85763283));
		line.add(new GeoPoint(33824680, -85763840));
		line.add(new GeoPoint(33825840, -85764000));
		line.add(new GeoPoint(33825871, -85766632));
		line.add(new GeoPoint(33824520, -85766693));
		line.add(new GeoPoint(33824291, -85766769));
		line.add(new GeoPoint(33824398, -85767036));
		line.add(new GeoPoint(33824429, -85767342));
		line.add(new GeoPoint(33824440, -85768692));
		line.add(new GeoPoint(33825890, -85768661));
		line.add(new GeoPoint(33825939, -85772469));
		line.add(new GeoPoint(33821678, -85772537));
		line.add(new GeoPoint(33820099, -85772522));
		line.add(new GeoPoint(33819248, -85772583));
		line.add(new GeoPoint(33818539, -85772850));
		line.add(new GeoPoint(33818539, -85768631));
		line.add(new GeoPoint(33819931, -85768784));
		line.add(new GeoPoint(33820160, -85768761));
		line.add(new GeoPoint(33820412, -85768333));
		line.add(new GeoPoint(33820889, -85767319));
		line.add(new GeoPoint(33821030, -85766899));
		line.add(new GeoPoint(33820950, -85766647));
		line.add(new GeoPoint(33820992, -85766289));

		return line;
	}
	
	public static ArrayList<GeoPoint> getGreenLine(){
		ArrayList<GeoPoint> line = new ArrayList<GeoPoint>();

		line.add(new GeoPoint(33821331, -85766342));
		line.add(new GeoPoint(33820980, -85766281));
		line.add(new GeoPoint(33821129, -85765442));
		line.add(new GeoPoint(33821270, -85765167));
		line.add(new GeoPoint(33821548, -85764801));
		line.add(new GeoPoint(33821861, -85764557));
		line.add(new GeoPoint(33822121, -85764488));
		line.add(new GeoPoint(33822632, -85764519));
		line.add(new GeoPoint(33822830, -85764412));
		line.add(new GeoPoint(33822861, -85763969));
		line.add(new GeoPoint(33823269, -85764572));
		line.add(new GeoPoint(33823448, -85764702));
		line.add(new GeoPoint(33823589, -85764732));
		line.add(new GeoPoint(33823841, -85764664));
		line.add(new GeoPoint(33823761, -85766167));
		line.add(new GeoPoint(33823601, -85766983));
		line.add(new GeoPoint(33823399, -85767616));
		line.add(new GeoPoint(33823250, -85767822));
		line.add(new GeoPoint(33823078, -85767990));
		line.add(new GeoPoint(33822739, -85768097));
		line.add(new GeoPoint(33822781, -85768494));
		line.add(new GeoPoint(33822762, -85768738));
		line.add(new GeoPoint(33824440, -85768700));
		line.add(new GeoPoint(33824421, -85767159));
		line.add(new GeoPoint(33824299, -85766769));
		line.add(new GeoPoint(33824532, -85766678));
		line.add(new GeoPoint(33825871, -85766632));
		line.add(new GeoPoint(33825851, -85764847));
		line.add(new GeoPoint(33827030, -85764839));
		line.add(new GeoPoint(33827000, -85763947));
		line.add(new GeoPoint(33829498, -85763863));
		line.add(new GeoPoint(33829441, -85763634));
		line.add(new GeoPoint(33829391, -85762749));
		line.add(new GeoPoint(33829769, -85761650));
		line.add(new GeoPoint(33829971, -85761726));
		line.add(new GeoPoint(33830639, -85761749));
		line.add(new GeoPoint(33830601, -85759750));
		line.add(new GeoPoint(33829231, -85759811));
		line.add(new GeoPoint(33826050, -85759727));
		line.add(new GeoPoint(33825981, -85760109));
		line.add(new GeoPoint(33825809, -85760674));
		line.add(new GeoPoint(33825821, -85762268));
		line.add(new GeoPoint(33825062, -85762482));
		line.add(new GeoPoint(33824951, -85763123));
		line.add(new GeoPoint(33824711, -85763069));
		line.add(new GeoPoint(33824810, -85762497));
		line.add(new GeoPoint(33824471, -85762451));
		line.add(new GeoPoint(33824478, -85761612));
		line.add(new GeoPoint(33825581, -85761238));
		line.add(new GeoPoint(33825809, -85761253));
		line.add(new GeoPoint(33825939, -85774391));
		line.add(new GeoPoint(33825901, -85775749));
		line.add(new GeoPoint(33825748, -85777382));
		line.add(new GeoPoint(33822220, -85777397));
		line.add(new GeoPoint(33822262, -85772682));
		line.add(new GeoPoint(33822250, -85772507));
		line.add(new GeoPoint(33821949, -85772537));
		line.add(new GeoPoint(33822189, -85772537));
		line.add(new GeoPoint(33822418, -85772537));
		line.add(new GeoPoint(33823620, -85772522));
		line.add(new GeoPoint(33823582, -85771927));
		line.add(new GeoPoint(33823399, -85770851));
		line.add(new GeoPoint(33823391, -85768723));
		line.add(new GeoPoint(33820160, -85768768));
		line.add(new GeoPoint(33820900, -85767311));
		line.add(new GeoPoint(33821018, -85766907));
		line.add(new GeoPoint(33820950, -85766640));
		line.add(new GeoPoint(33820980, -85766281));
		
		return line;
	}
	
	public static ArrayList<GeoPoint> getRedStops(){
		ArrayList<GeoPoint> stops = new ArrayList<GeoPoint>();
		
		stops.add(new GeoPoint(33821327, -85766342));
		stops.add(new GeoPoint(33822067, -85764519));
		stops.add(new GeoPoint(33823730, -85766411));
		stops.add(new GeoPoint(33823063, -85767982));
		stops.add(new GeoPoint(33820850, -85768379));
		stops.add(new GeoPoint(33822323, -85768730));
		stops.add(new GeoPoint(33824867, -85768669));
		stops.add(new GeoPoint(33827003, -85764359));
		stops.add(new GeoPoint(33829391, -85762672));
		stops.add(new GeoPoint(33829967, -85761719));
		stops.add(new GeoPoint(33829460, -85759796));
		stops.add(new GeoPoint(33825817, -85761497));
		stops.add(new GeoPoint(33824718, -85763069));
		stops.add(new GeoPoint(33825100, -85761398));
		stops.add(new GeoPoint(33823170, -85762093));
		
		return stops;
	}
	
	public static ArrayList<GeoPoint> getWhiteStops(){
		ArrayList<GeoPoint> stops = new ArrayList<GeoPoint>();
		
		stops.add(new GeoPoint(33821320, -85766335));
		stops.add(new GeoPoint(33822079, -85764511));
		stops.add(new GeoPoint(33822002, -85763893));
		stops.add(new GeoPoint(33820778, -85763451));
		stops.add(new GeoPoint(33818520, -85762634));
		stops.add(new GeoPoint(33819168, -85765739));
		stops.add(new GeoPoint(33820034, -85768784));
		stops.add(new GeoPoint(33822350, -85768730));
		stops.add(new GeoPoint(33824875, -85768669));
		stops.add(new GeoPoint(33825298, -85766663));
		stops.add(new GeoPoint(33823738, -85766403));
		stops.add(new GeoPoint(33823074, -85767975));
		
		return stops;
	}
	
	public static ArrayList<GeoPoint> getPurpleStops(){
		ArrayList<GeoPoint> stops = new ArrayList<GeoPoint>();
		
		stops.add(new GeoPoint(33821335, -85766335));
		stops.add(new GeoPoint(33822071, -85764519));
		stops.add(new GeoPoint(33822025, -85763893));
		stops.add(new GeoPoint(33820770, -85763451));
		stops.add(new GeoPoint(33825813, -85761497));
		stops.add(new GeoPoint(33824711, -85763062));
		stops.add(new GeoPoint(33825092, -85761398));
		stops.add(new GeoPoint(33829967, -85761711));
		stops.add(new GeoPoint(33829399, -85762672));
		stops.add(new GeoPoint(33827003, -85764359));
		stops.add(new GeoPoint(33824875, -85768669));
		stops.add(new GeoPoint(33823734, -85766403));
		stops.add(new GeoPoint(33823067, -85767975));
		
		return stops;
	}
	
	public static ArrayList<GeoPoint> getBlueStops(){
		ArrayList<GeoPoint> stops = new ArrayList<GeoPoint>();
		
		stops.add(new GeoPoint(33821323, -85766335));
		stops.add(new GeoPoint(33822075, -85764511));
		stops.add(new GeoPoint(33814884, -85765099));
		stops.add(new GeoPoint(33815315, -85762749));
		stops.add(new GeoPoint(33803665, -85762482));
		stops.add(new GeoPoint(33802864, -85762482));
		stops.add(new GeoPoint(33791046, -85761253));
		stops.add(new GeoPoint(33788376, -85762634));
		stops.add(new GeoPoint(33805302, -85757416));
		stops.add(new GeoPoint(33805759, -85754936));
		stops.add(new GeoPoint(33809372, -85753471));
		stops.add(new GeoPoint(33807163, -85760872));
		stops.add(new GeoPoint(33813793, -85759811));
		stops.add(new GeoPoint(33816319, -85760689));
		stops.add(new GeoPoint(33823734, -85766418));
		stops.add(new GeoPoint(33823078, -85767982));
		
		return stops;
	}
	
	public static ArrayList<GeoPoint> getBlackStops(){
		ArrayList<GeoPoint> stops = new ArrayList<GeoPoint>();
		
		stops.add(new GeoPoint(33821327, -85766342));
		stops.add(new GeoPoint(33822083, -85764526));
		stops.add(new GeoPoint(33813431, -85761887));
		stops.add(new GeoPoint(33802860, -85762482));
		stops.add(new GeoPoint(33803658, -85762489));
		stops.add(new GeoPoint(33804920, -85762505));
		stops.add(new GeoPoint(33807163, -85760864));
		stops.add(new GeoPoint(33816326, -85760689));
		stops.add(new GeoPoint(33823738, -85766411));
		stops.add(new GeoPoint(33791046, -85761246));
		stops.add(new GeoPoint(33788383, -85762634));
		
		return stops;
	}
	
	public static ArrayList<GeoPoint> getYellowStops(){
		ArrayList<GeoPoint> stops = new ArrayList<GeoPoint>();
		
		stops.add(new GeoPoint(33821327, -85766335));
		stops.add(new GeoPoint(33822083, -85764519));
		stops.add(new GeoPoint(33813427, -85761879));
		stops.add(new GeoPoint(33812531, -85761307));
		stops.add(new GeoPoint(33816322, -85760689));
		stops.add(new GeoPoint(33825294, -85766655));
		stops.add(new GeoPoint(33824875, -85768677));
		stops.add(new GeoPoint(33824612, -85772491));
		stops.add(new GeoPoint(33823574, -85771957));
		stops.add(new GeoPoint(33822350, -85768730));

		
		return stops;
	}
	
	public static ArrayList<GeoPoint> getGreenStops(){
		ArrayList<GeoPoint> stops = new ArrayList<GeoPoint>();
		
		stops.add(new GeoPoint(33821327, -85766335));
		stops.add(new GeoPoint(33822071, -85764519));
		stops.add(new GeoPoint(33823734, -85766418));
		stops.add(new GeoPoint(33823071, -85767982));
		stops.add(new GeoPoint(33825279, -85766655));
		stops.add(new GeoPoint(33827007, -85764359));
		stops.add(new GeoPoint(33829395, -85762672));
		stops.add(new GeoPoint(33829964, -85761711));
		stops.add(new GeoPoint(33824711, -85763069));
		stops.add(new GeoPoint(33825935, -85773705));
		stops.add(new GeoPoint(33820518, -85768089));
		stops.add(new GeoPoint(33823563, -85771957));
		
		return stops;
	}
	
	public static int getRedColor(){
		return Color.argb(75, 240, 0, 0);
	}
	
	public static int getWhiteColor(){
		return Color.argb(50, 50, 50, 50);
	}
	
	public static int getPurpleColor(){
		return Color.argb(75, 120, 0, 120);
	}
	
	public static int getBlueColor(){
		return Color.argb(75, 0, 0, 240);
	}
	
	public static int getBlackColor(){
		return Color.argb(75, 0, 0, 0);
	}
	
	public static int getYellowColor(){
		return Color.argb(125, 200, 200, 0);
	}
	
	public static int getGreenColor(){
		return Color.argb(75, 0, 120, 0);
	}
}
