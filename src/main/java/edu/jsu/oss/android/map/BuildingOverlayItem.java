/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import android.graphics.drawable.Drawable;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.GeoPoint;

public class BuildingOverlayItem extends OverlayItem {
	
	private Drawable icon;
	
	public BuildingOverlayItem(GeoPoint point, String title, String snippet, Drawable icon) {
		super(point, title, snippet);
		this.icon = icon;
		//this.icon.setBounds(0, 0, icon.getIntrinsicWidth(), icon.getIntrinsicHeight());
	}
	
	@Override
	public Drawable getMarker(int stateBitset){
		return icon;
	}
}
