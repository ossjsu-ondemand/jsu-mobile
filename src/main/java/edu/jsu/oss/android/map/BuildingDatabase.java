/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.io.IOException;
import java.util.HashMap;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;

/**
 * Contains logic to return specific words from the dictionary, and
 * load the dictionary table when it needs to be created.
 */
public class BuildingDatabase {
    /** The campus building name column, or the "word" column in the sense of a dictionary. */
    public static final String KEY_WORD = SearchManager.SUGGEST_COLUMN_TEXT_1;
    /** The campus building description column, or the "definition" column in the sense of a dictionary. */
    public static final String KEY_DEFINITION = SearchManager.SUGGEST_COLUMN_TEXT_2;
    /** The column defining the latitude component of the GeoPoint describing the building */
    public static final String KEY_LAT = "LAT";
    /** The column defining the longitude component of the GeoPoint describing the building */
    public static final String KEY_LON = "LON";

    /** Name of the database where the building information is stored.  This is often referred to as
     * a dictionary since it uses word/definition pairs that map to building/description pairs. */
    private static final String DATABASE_NAME = "dictionary";
    /** Used for the full text search (FTS), which is performed using a virtual table instead of a 
     * regular query. */
    private static final String FTS_VIRTUAL_TABLE = "FTSdictionary";
    /** Current version of the database. */
    private static final int DATABASE_VERSION = 2;

    /** Used to create and open the database. */
    private final DictionaryOpenHelper mDatabaseOpenHelper;
    /** Map used to reference database columns */
    private static final HashMap<String,String> mColumnMap = buildColumnMap();

    /**
     * Creates a BuildingDatabase object.
     * 
     * @param context The Context within which to work, used to create the DB
     */
    public BuildingDatabase(Context context) {
        mDatabaseOpenHelper = new DictionaryOpenHelper(context);
    }

    /**
     * Builds a map for all columns that may be requested, which will be given to the 
     * SQLiteQueryBuilder. This is a good way to define aliases for column names, but must include 
     * all columns, even if the value is the key. This allows the ContentProvider to request
     * columns w/o the need to know real column names and create the alias itself.
     */
    private static HashMap<String,String> buildColumnMap() {
        HashMap<String,String> map = new HashMap<String,String>();
        map.put(KEY_WORD, KEY_WORD);
        map.put(KEY_DEFINITION, KEY_DEFINITION);
        map.put(KEY_LAT, KEY_LAT);
        map.put(KEY_LON, KEY_LON);
        map.put(BaseColumns._ID, "rowid AS " +
                BaseColumns._ID);
        map.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID, "rowid AS " +
                SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
        map.put(SearchManager.SUGGEST_COLUMN_SHORTCUT_ID, "rowid AS " +
                SearchManager.SUGGEST_COLUMN_SHORTCUT_ID);
        return map;
    }

    /**
     * Returns a Cursor positioned at the word specified by rowId
     *
     * @param rowId id of word to retrieve
     * @param columns The columns to include, if null then all are included
     * @return Cursor positioned to matching word, or null if not found.
     */
    public Cursor getWord(String rowId, String[] columns) {
        String selection = "rowid = ?";
        String[] selectionArgs = new String[] {rowId};

        return query(selection, selectionArgs, columns);
    }

    /**
     * Returns a Cursor over all words that match the given query
     *
     * @param query The string to search for
     * @param columns The columns to include, if null then all are included
     * @return Cursor over all words that match, or null if none found.
     */
    public Cursor getWordMatches(String query, String[] columns) {
        String selection = KEY_WORD + " MATCH ?";
        String[] selectionArgs = new String[] {query+"*"};

        return query(selection, selectionArgs, columns);
    }

    /**
     * Performs a database query.
     * 
     * @param selection The selection clause
     * @param selectionArgs Selection arguments for "?" components in the selection
     * @param columns The columns to return
     * @return A Cursor over all rows matching the query
     */
    private Cursor query(String selection, String[] selectionArgs, String[] columns) {
        /* The SQLiteBuilder provides a map for all possible columns requested to
         * actual columns in the database, creating a simple column alias mechanism
         * by which the ContentProvider does not need to know the real column names
         */
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(FTS_VIRTUAL_TABLE);
        builder.setProjectionMap(mColumnMap);

        Cursor cursor = builder.query(mDatabaseOpenHelper.getReadableDatabase(),
                columns, selection, selectionArgs, null, null, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;
    }


    /**
     * This creates/opens the database.
     */
    private static class DictionaryOpenHelper extends SQLiteOpenHelper {

        private SQLiteDatabase mDatabase;

        /* Note that FTS3 does not support column constraints and thus, you cannot
         * declare a primary key. However, "rowid" is automatically used as a unique
         * identifier, so when making requests, we will use "_id" as an alias for "rowid"
         */
        private static final String FTS_TABLE_CREATE =
                    "CREATE VIRTUAL TABLE " + FTS_VIRTUAL_TABLE +
                    " USING fts3 (" +
                    KEY_WORD + ", " +
                    KEY_DEFINITION + "," +
                    KEY_LAT + "," +
                    KEY_LON + ");";

        /**
         * Creates a new DictionaryOpenHelper.
         * 
         * @param context BuildingDatabase object that created this object.
         */
        DictionaryOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        /**
         * Creates the database table and loads the dictionary with "word" representing building/description
         * pairs.
         * 
         * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
         */
        @Override
        public void onCreate(SQLiteDatabase db) {
            mDatabase = db;
            mDatabase.execSQL(FTS_TABLE_CREATE);
            loadDictionary();
        }

        /**
         * Starts a thread to load the database table with words
         */
        private void loadDictionary() {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        loadWords();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }).start();
        }

        /**
         * loads each building into the dictionary list
         * 
         * @throws IOException
         */
        private void loadWords() throws IOException {
            
        	addWord("Daugette Hall", "Daugette Hall, one of JSU's earliest dormitories, has been converted to office use for the computer center and office of continuing education.", "33821438", "-85765610");
			addWord("Jack Hopper Dining Hall", "Buffet-Style Cafeteria offering a variety of food including home-style entrees, pizza and pasta bar, salads, and deli subs.", "33822308", "-85766563");
			addWord("Leon Cole Auditorium", "Auditorium for assemblies, conferences, and luncheons.", "33822090", "-85766769");
			addWord("Anders Hall Round House", "Commonly referred to as the \"Round House,\" this is a common meeting and conference facility.", "33821999", "-85765190");
			addWord("Bibb Graves Hall", "Bibb Graves Hall is the University's administration building and one of the oldest facilities on the main campus.", "33823044", "-85765282");
			addWord("Ramona Wood Hall", "The Romana Wood Building is the home of the College of Education and Professional Studies.", "33823391", "-85766693");
			addWord("Ayers Hall", "Ayers Hall is home to Computer Science, Mathematics, Technology, and Psychology.", "33824089", "-85765823");
			addWord("Sparkman Hall", "Standing nine stories tall, this Co-ed Dormitory is a landmark on campus.", "33823799", "-85767006");
			addWord("Hammond Hall", "Hammond Hall on University Circle is the home of JSU&#39;s Department of Art and the Hammond Hall Art Gallery.", "33822117", "-85764885");
			addWord("Computer Center", "Home of university computer and network support staff.", "33821812", "-85764900");
			addWord("Theron Montgomery Building - TMB", "More often referred to as the &quot;TMB,&quot; this student commons building houses the mail center, tutoring services, print shop, and office of student activities.", "33821911", "-85764282");
			addWord("Theron Montgomery Food Court", "Campus Chick-Fil-A,  Baja Fresh Mexican Grill, and 155 Degrees restaurants.  Flex dollars are always welcome here!", "33822124", "-85764130");
			addWord("Campus Bookstore", "Offerings in course textbooks as well as school supplies and JSU apparel.", "33822319", "-85764244");
			addWord("Stephenson Hall", "Student recreation facility complete with basketball courts, weight rooms, pool tables, racquetball courts, ping-pong, and steam rooms.", "33820854", "-85764061");
			addWord("Kennamer Hall", "Athletic administration offices as well as varsity sport weight room and training facilities.", "33820152", "-85763557");
			addWord("Wesley House", "Methodist college ministry with residence halls.", "33819668", "-85762138");
			addWord("Colonial Arms Apartments", "one and two bedroom apartments requiring sophomore status for application.", "33818947", "-85762718");
			addWord("Stadium Tower Dormitory", "Four bedroom suite with kitchen, common area, and two bathrooms.  Requires Sophomore status for residency application.", "33819695", "-85766266");
			addWord("Gamecock Field House", "Football administrative offices, meetings rooms, locker, and equipment facilities.", "33820377", "-85767303");
			addWord("Burgess-Snow Football Field", "Home of Gamecock Football, the 24,000 seat facility is also used for graduation and other special events.", "33820343", "-85766457");
			addWord("Fitzpatrick Hall Dormitory", "Female freshmen dormitory with double occupancy rooms and community baths.", "33821392", "-85767029");
			addWord("Curtiss Hall Dormitory", "Female freshmen dormitory for those interested in Greek life.  Curtiss Hall has double occupancy rooms and community baths.", "33820717", "-85768181");
			addWord("Salls Hall - Police Department", "University Police Department and English Language Institute", "33820213", "-85769196");
			addWord("Campus Inn Apartments", "Efficiency and one bedroom apartments requiring sophomore status at time of application.", "33819538", "-85769371");
			addWord("Jax Apartments", "One and two bedroom unfurnished apartments for families and married couples.", "33819561", "-85771141");
			addWord("Penn House Apartments", "One bedroom apartments requiring sophomore status at the time of application.", "33821793", "-85776939");
			addWord("Gamecock Track", "Standard 400 yd track with fully equipped infield.", "33822704", "-85775696");
			addWord("Rudy Abbott Baseball Field", "Home of the two-time national champion Gamecock Baseball team.", "33825050", "-85776596");
			addWord("JSU Soccer Field", "Built in 2003, this is the women's soccer facility.", "33824982", "-85775032");
			addWord("Pete Matthews Coliseum", "Functions as the arena for both men's and women's basketball in addition to housing an indoor pool, weight facilities, and exercise classes.", "33824093", "-85773598");
			addWord("JSU Tennis Complex", "Lighted complex with fifteen tennis courts.", "33822887", "-85773178");
			addWord("Pannell Hall Apartments", "Efficiency apartments conveniently located in the center of campus.  These require sophomore status at the time of application.", "33822346", "-85767487");
			addWord("Student Health Center", "Offers free medical care to full-time students of JSU.", "33823692", "-85767540");
			addWord("Mason Hall", "Facilities for the Department of Music and Department of Family and Consumer Sciences.  Home of the Marching Southerner and Ballerinas, Mason Hall also features a concert hall.", "33823090", "-85768364");
			addWord("Carlisle Fine Arts Facility", "Arts building with digital and film laboratories as well as painting, drawing, and sculpting studios.", "33823662", "-85768433");
			addWord("Self Hall", "Houses the Department of Communications, Office of Distance Education, and TV Services.", "33824833", "-85768990");
			addWord("Dixon Hall Dormitory", "Freshman male dormitory with double occupancy rooms and community baths.", "33824875", "-85768417");
			addWord("Crow Hall Dormitory", "Freshman male dormitory with double occupancy rooms and community baths.", "33824875", "-85767944");
			addWord("Patterson Hall Dormitory", "Male dormitory for \"Cocky Experience\" and honors students.  Double occupancy rooms with private bath.", "33825344", "-85767540");
			addWord("Logan Hall", "Female dormitory for \"Cocky Experience\" and honors students.  Double occupancy rooms with private bath.", "33825333", "-85766045");
			addWord("International House Dormitory", "Dormitory for the International House student program.", "33825352", "-85764763");
			addWord("Duncan Maintenance Shop", "Service and repair building for University property.", "33823734", "-85769279");
			addWord("Visitor Center", "Provides campus tours and general information about JSU.", "33826134", "-85764503");
			addWord("Baptist Campus Ministries - BCM", "Baptist college ministry for students.", "33826180", "-85763733");
			addWord("University Softball Field", "Home of Gamecock Softball with a seating capacity of 1,000 people.", "33826443", "-85762794");
			addWord("Wallace Hall", "Home to the renowned nursing program of JSU.", "33826344", "-85761513");
			addWord("Rowe Hall", "Military Science department and ROTC program facility.", "33826614", "-85760040");
			addWord("Rifle Range", "Facility for JSU's 3-time National Champion rifle team.", "33826611", "-85760361");
			addWord("Merrill Hall", "Home to the College of Commerce and Business Administration", "33828590", "-85763245");
			addWord("Ampitheater", "Outdoor stage for JSU's drama and musical productions.", "33830257", "-85762970");
			addWord("Ernest Stone Center", "Houses the Art, History, English, Drama, and Music Departments as well as a production stage.", "33830250", "-85760658");
			addWord("Brewer Hall", "Home to the Sociology and Social Work, Criminal Justine, and Political Science and Public Administration Departments.", "33827286", "-85763504");
			addWord("Martin Hall", "Home for Biology, Chemistry, Physics, and other related sciences.", "33825275", "-85763367");
			addWord("McGee Science Center", "Science laboratory and research facility.", "33825459", "-85762833");
			addWord("Houston Cole Library", "700,000 volume library with conference center, computer labs, and study floor.", "33824188", "-85763382");
			addWord("Jazzman's Cafe", "Coffee Shop and Cafe will provide the brain fuel for the late nights at the library!", "33824249", "-85763100");
			addWord("College Apartments", "One bedroom apartments requiring sophomore status at the time of application.", "33823376", "-85762215");
			addWord("Alumni House", "Facility used for social and cultural events by JSU or Alumni.", "33822731", "-85762138");
			addWord("Gamecock Diner", "Flex dining facility for JSU students.", "33820721", "-85764603");
			addWord("WOW Cafe & Wingery", "Offers a wide variety of wings, wraps, chicken, and special sauces.", "33822147", "-85766380");

        }

        /**
         * Add a word to the dictionary.
         * 
         * @return rowId or -1 if failed
         */
        public long addWord(String word, String definition, String lat, String lon) {
            ContentValues initialValues = new ContentValues();
            initialValues.put(KEY_WORD, word);
            initialValues.put(KEY_DEFINITION, definition);
            initialValues.put(KEY_LAT, lat);
            initialValues.put(KEY_LON, lon);

            return mDatabase.insert(FTS_VIRTUAL_TABLE, null, initialValues);
        }

        /**
         * If a database upgrade occurs, delete and reload the database.
         * 
         * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + FTS_VIRTUAL_TABLE);
            onCreate(db);
        }
    }

}
