/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.util.ArrayList;

import com.google.android.maps.GeoPoint;

public final class ParkingPolygons {

	public static ArrayList<ArrayList<GeoPoint>> getSilverParking(){
		ArrayList<ArrayList<GeoPoint>> parking = new ArrayList<ArrayList<GeoPoint>>();

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33823814, -85764748));
		parking.get(parking.size() - 1).add(new GeoPoint(33823658, -85764809));
		parking.get(parking.size() - 1).add(new GeoPoint(33823517, -85764832));
		parking.get(parking.size() - 1).add(new GeoPoint(33823380, -85764786));
		parking.get(parking.size() - 1).add(new GeoPoint(33823257, -85764687));
		parking.get(parking.size() - 1).add(new GeoPoint(33823135, -85764565));
		parking.get(parking.size() - 1).add(new GeoPoint(33823048, -85764412));
		parking.get(parking.size() - 1).add(new GeoPoint(33822933, -85764160));
		parking.get(parking.size() - 1).add(new GeoPoint(33822910, -85764099));
		parking.get(parking.size() - 1).add(new GeoPoint(33823025, -85764015));
		parking.get(parking.size() - 1).add(new GeoPoint(33823162, -85764343));
		parking.get(parking.size() - 1).add(new GeoPoint(33823265, -85764496));
		parking.get(parking.size() - 1).add(new GeoPoint(33823448, -85764641));
		parking.get(parking.size() - 1).add(new GeoPoint(33823643, -85764626));
		parking.get(parking.size() - 1).add(new GeoPoint(33823814, -85764587));
		parking.get(parking.size() - 1).add(new GeoPoint(33823814, -85764748));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33825752, -85762154));
		parking.get(parking.size() - 1).add(new GeoPoint(33824989, -85762314));
		parking.get(parking.size() - 1).add(new GeoPoint(33824181, -85762291));
		parking.get(parking.size() - 1).add(new GeoPoint(33824562, -85761322));
		parking.get(parking.size() - 1).add(new GeoPoint(33825031, -85761299));
		parking.get(parking.size() - 1).add(new GeoPoint(33825050, -85760170));
		parking.get(parking.size() - 1).add(new GeoPoint(33825577, -85760010));
		parking.get(parking.size() - 1).add(new GeoPoint(33825581, -85761093));
		parking.get(parking.size() - 1).add(new GeoPoint(33825760, -85761063));
		parking.get(parking.size() - 1).add(new GeoPoint(33825752, -85762154));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33827564, -85760101));
		parking.get(parking.size() - 1).add(new GeoPoint(33827286, -85760101));
		parking.get(parking.size() - 1).add(new GeoPoint(33827286, -85760681));
		parking.get(parking.size() - 1).add(new GeoPoint(33827564, -85760689));
		parking.get(parking.size() - 1).add(new GeoPoint(33827564, -85760101));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33829166, -85761475));
		parking.get(parking.size() - 1).add(new GeoPoint(33828991, -85761429));
		parking.get(parking.size() - 1).add(new GeoPoint(33829128, -85760567));
		parking.get(parking.size() - 1).add(new GeoPoint(33829121, -85760048));
		parking.get(parking.size() - 1).add(new GeoPoint(33829334, -85760033));
		parking.get(parking.size() - 1).add(new GeoPoint(33829330, -85760590));
		parking.get(parking.size() - 1).add(new GeoPoint(33829166, -85761475));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33829449, -85760040));
		parking.get(parking.size() - 1).add(new GeoPoint(33829441, -85760612));
		parking.get(parking.size() - 1).add(new GeoPoint(33829304, -85761635));
		parking.get(parking.size() - 1).add(new GeoPoint(33829399, -85761856));
		parking.get(parking.size() - 1).add(new GeoPoint(33829231, -85762337));
		parking.get(parking.size() - 1).add(new GeoPoint(33829430, -85762413));
		parking.get(parking.size() - 1).add(new GeoPoint(33829258, -85762894));
		parking.get(parking.size() - 1).add(new GeoPoint(33829296, -85763535));
		parking.get(parking.size() - 1).add(new GeoPoint(33829712, -85763489));
		parking.get(parking.size() - 1).add(new GeoPoint(33829659, -85762848));
		parking.get(parking.size() - 1).add(new GeoPoint(33829731, -85762589));
		parking.get(parking.size() - 1).add(new GeoPoint(33830627, -85762581));
		parking.get(parking.size() - 1).add(new GeoPoint(33830605, -85761803));
		parking.get(parking.size() - 1).add(new GeoPoint(33830002, -85761803));
		parking.get(parking.size() - 1).add(new GeoPoint(33829945, -85761940));
		parking.get(parking.size() - 1).add(new GeoPoint(33829700, -85761818));
		parking.get(parking.size() - 1).add(new GeoPoint(33829800, -85761513));
		parking.get(parking.size() - 1).add(new GeoPoint(33829861, -85760574));
		parking.get(parking.size() - 1).add(new GeoPoint(33829842, -85760040));
		parking.get(parking.size() - 1).add(new GeoPoint(33829449, -85760040));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33825775, -85772667));
		parking.get(parking.size() - 1).add(new GeoPoint(33824505, -85772675));
		parking.get(parking.size() - 1).add(new GeoPoint(33824532, -85774246));
		parking.get(parking.size() - 1).add(new GeoPoint(33823708, -85774246));
		parking.get(parking.size() - 1).add(new GeoPoint(33823696, -85772926));
		parking.get(parking.size() - 1).add(new GeoPoint(33824505, -85772942));
		parking.get(parking.size() - 1).add(new GeoPoint(33824497, -85772667));
		parking.get(parking.size() - 1).add(new GeoPoint(33823456, -85772675));
		parking.get(parking.size() - 1).add(new GeoPoint(33823483, -85774300));
		parking.get(parking.size() - 1).add(new GeoPoint(33823490, -85774506));
		parking.get(parking.size() - 1).add(new GeoPoint(33825787, -85774551));
		parking.get(parking.size() - 1).add(new GeoPoint(33825775, -85772667));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33822895, -85763206));
		parking.get(parking.size() - 1).add(new GeoPoint(33822506, -85763023));
		parking.get(parking.size() - 1).add(new GeoPoint(33822430, -85763809));
		parking.get(parking.size() - 1).add(new GeoPoint(33822742, -85763885));
		parking.get(parking.size() - 1).add(new GeoPoint(33822895, -85763206));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33821732, -85763733));
		parking.get(parking.size() - 1).add(new GeoPoint(33821457, -85763634));
		parking.get(parking.size() - 1).add(new GeoPoint(33821732, -85762672));
		parking.get(parking.size() - 1).add(new GeoPoint(33821964, -85762756));
		parking.get(parking.size() - 1).add(new GeoPoint(33821732, -85763733));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33821308, -85763618));
		parking.get(parking.size() - 1).add(new GeoPoint(33821232, -85763519));
		parking.get(parking.size() - 1).add(new GeoPoint(33821629, -85762779));
		parking.get(parking.size() - 1).add(new GeoPoint(33821648, -85762878));
		parking.get(parking.size() - 1).add(new GeoPoint(33821308, -85763618));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33821117, -85763557));
		parking.get(parking.size() - 1).add(new GeoPoint(33820820, -85763428));
		parking.get(parking.size() - 1).add(new GeoPoint(33820942, -85762299));
		parking.get(parking.size() - 1).add(new GeoPoint(33821594, -85762596));
		parking.get(parking.size() - 1).add(new GeoPoint(33821117, -85763557));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33828159, -85764008));
		parking.get(parking.size() - 1).add(new GeoPoint(33827076, -85764038));
		parking.get(parking.size() - 1).add(new GeoPoint(33827099, -85764732));
		parking.get(parking.size() - 1).add(new GeoPoint(33828178, -85764709));
		parking.get(parking.size() - 1).add(new GeoPoint(33828159, -85764008));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33826950, -85764717));
		parking.get(parking.size() - 1).add(new GeoPoint(33826668, -85764732));
		parking.get(parking.size() - 1).add(new GeoPoint(33826630, -85764091));
		parking.get(parking.size() - 1).add(new GeoPoint(33826923, -85764069));
		parking.get(parking.size() - 1).add(new GeoPoint(33826950, -85764717));

		return parking;
	}
	
	public static ArrayList<ArrayList<GeoPoint>> getGoldParking(){
		ArrayList<ArrayList<GeoPoint>> parking = new ArrayList<ArrayList<GeoPoint>>();

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33823803, -85764771));
		parking.get(parking.size() - 1).add(new GeoPoint(33823517, -85764862));
		parking.get(parking.size() - 1).add(new GeoPoint(33823364, -85764824));
		parking.get(parking.size() - 1).add(new GeoPoint(33823132, -85764603));
		parking.get(parking.size() - 1).add(new GeoPoint(33823036, -85764420));
		parking.get(parking.size() - 1).add(new GeoPoint(33822941, -85764221));
		parking.get(parking.size() - 1).add(new GeoPoint(33822861, -85764481));
		parking.get(parking.size() - 1).add(new GeoPoint(33823006, -85764755));
		parking.get(parking.size() - 1).add(new GeoPoint(33823219, -85764977));
		parking.get(parking.size() - 1).add(new GeoPoint(33823448, -85765060));
		parking.get(parking.size() - 1).add(new GeoPoint(33823658, -85765030));
		parking.get(parking.size() - 1).add(new GeoPoint(33823803, -85764771));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33829971, -85761673));
		parking.get(parking.size() - 1).add(new GeoPoint(33829964, -85761528));
		parking.get(parking.size() - 1).add(new GeoPoint(33830292, -85761513));
		parking.get(parking.size() - 1).add(new GeoPoint(33830280, -85760956));
		parking.get(parking.size() - 1).add(new GeoPoint(33830379, -85760956));
		parking.get(parking.size() - 1).add(new GeoPoint(33830379, -85760864));
		parking.get(parking.size() - 1).add(new GeoPoint(33830517, -85760864));
		parking.get(parking.size() - 1).add(new GeoPoint(33830532, -85761658));
		parking.get(parking.size() - 1).add(new GeoPoint(33829971, -85761673));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33829876, -85760193));
		parking.get(parking.size() - 1).add(new GeoPoint(33829872, -85759995));
		parking.get(parking.size() - 1).add(new GeoPoint(33830479, -85759979));
		parking.get(parking.size() - 1).add(new GeoPoint(33830482, -85760185));
		parking.get(parking.size() - 1).add(new GeoPoint(33829876, -85760193));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33829426, -85762413));
		parking.get(parking.size() - 1).add(new GeoPoint(33829239, -85762344));
		parking.get(parking.size() - 1).add(new GeoPoint(33829090, -85762871));
		parking.get(parking.size() - 1).add(new GeoPoint(33829121, -85763535));
		parking.get(parking.size() - 1).add(new GeoPoint(33829292, -85763527));
		parking.get(parking.size() - 1).add(new GeoPoint(33829262, -85762894));
		parking.get(parking.size() - 1).add(new GeoPoint(33829426, -85762413));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33827038, -85763374));
		parking.get(parking.size() - 1).add(new GeoPoint(33827507, -85763344));
		parking.get(parking.size() - 1).add(new GeoPoint(33827496, -85763077));
		parking.get(parking.size() - 1).add(new GeoPoint(33827023, -85763107));
		parking.get(parking.size() - 1).add(new GeoPoint(33827038, -85763374));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33829052, -85763763));
		parking.get(parking.size() - 1).add(new GeoPoint(33828938, -85763809));
		parking.get(parking.size() - 1).add(new GeoPoint(33828709, -85763680));
		parking.get(parking.size() - 1).add(new GeoPoint(33828476, -85763695));
		parking.get(parking.size() - 1).add(new GeoPoint(33828224, -85763870));
		parking.get(parking.size() - 1).add(new GeoPoint(33828083, -85763885));
		parking.get(parking.size() - 1).add(new GeoPoint(33828438, -85763603));
		parking.get(parking.size() - 1).add(new GeoPoint(33828716, -85763588));
		parking.get(parking.size() - 1).add(new GeoPoint(33829052, -85763763));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33826077, -85761841));
		parking.get(parking.size() - 1).add(new GeoPoint(33826080, -85761971));
		parking.get(parking.size() - 1).add(new GeoPoint(33826649, -85761955));
		parking.get(parking.size() - 1).add(new GeoPoint(33826637, -85761200));
		parking.get(parking.size() - 1).add(new GeoPoint(33826572, -85761192));
		parking.get(parking.size() - 1).add(new GeoPoint(33826588, -85761833));
		parking.get(parking.size() - 1).add(new GeoPoint(33826077, -85761841));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33826756, -85760139));
		parking.get(parking.size() - 1).add(new GeoPoint(33826752, -85760429));
		parking.get(parking.size() - 1).add(new GeoPoint(33826996, -85760437));
		parking.get(parking.size() - 1).add(new GeoPoint(33826996, -85760132));
		parking.get(parking.size() - 1).add(new GeoPoint(33826756, -85760139));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33825249, -85762535));
		parking.get(parking.size() - 1).add(new GeoPoint(33825672, -85762421));
		parking.get(parking.size() - 1).add(new GeoPoint(33825665, -85762360));
		parking.get(parking.size() - 1).add(new GeoPoint(33825241, -85762482));
		parking.get(parking.size() - 1).add(new GeoPoint(33825249, -85762535));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33824932, -85763245));
		parking.get(parking.size() - 1).add(new GeoPoint(33824612, -85763176));
		parking.get(parking.size() - 1).add(new GeoPoint(33824692, -85762657));
		parking.get(parking.size() - 1).add(new GeoPoint(33824238, -85762596));
		parking.get(parking.size() - 1).add(new GeoPoint(33824265, -85762543));
		parking.get(parking.size() - 1).add(new GeoPoint(33824703, -85762589));
		parking.get(parking.size() - 1).add(new GeoPoint(33825031, -85762634));
		parking.get(parking.size() - 1).add(new GeoPoint(33824932, -85763245));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33825172, -85764626));
		parking.get(parking.size() - 1).add(new GeoPoint(33825111, -85764626));
		parking.get(parking.size() - 1).add(new GeoPoint(33825108, -85764763));
		parking.get(parking.size() - 1).add(new GeoPoint(33824932, -85764618));
		parking.get(parking.size() - 1).add(new GeoPoint(33824959, -85764328));
		parking.get(parking.size() - 1).add(new GeoPoint(33825172, -85764626));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33823906, -85765221));
		parking.get(parking.size() - 1).add(new GeoPoint(33824581, -85765152));
		parking.get(parking.size() - 1).add(new GeoPoint(33824642, -85764946));
		parking.get(parking.size() - 1).add(new GeoPoint(33823963, -85765030));
		parking.get(parking.size() - 1).add(new GeoPoint(33823906, -85765221));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33823406, -85767319));
		parking.get(parking.size() - 1).add(new GeoPoint(33823048, -85767471));
		parking.get(parking.size() - 1).add(new GeoPoint(33822685, -85767212));
		parking.get(parking.size() - 1).add(new GeoPoint(33822701, -85766991));
		parking.get(parking.size() - 1).add(new GeoPoint(33822987, -85767250));
		parking.get(parking.size() - 1).add(new GeoPoint(33823074, -85767082));
		parking.get(parking.size() - 1).add(new GeoPoint(33823406, -85767319));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33821125, -85763557));
		parking.get(parking.size() - 1).add(new GeoPoint(33821049, -85763641));
		parking.get(parking.size() - 1).add(new GeoPoint(33820763, -85763519));
		parking.get(parking.size() - 1).add(new GeoPoint(33820820, -85763435));
		parking.get(parking.size() - 1).add(new GeoPoint(33821125, -85763557));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33820690, -85763725));
		parking.get(parking.size() - 1).add(new GeoPoint(33821049, -85763870));
		parking.get(parking.size() - 1).add(new GeoPoint(33821114, -85763809));
		parking.get(parking.size() - 1).add(new GeoPoint(33820747, -85763672));
		parking.get(parking.size() - 1).add(new GeoPoint(33820690, -85763725));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33820686, -85763397));
		parking.get(parking.size() - 1).add(new GeoPoint(33820751, -85763329));
		parking.get(parking.size() - 1).add(new GeoPoint(33820786, -85762962));
		parking.get(parking.size() - 1).add(new GeoPoint(33820721, -85763031));
		parking.get(parking.size() - 1).add(new GeoPoint(33820686, -85763397));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33822041, -85763786));
		parking.get(parking.size() - 1).add(new GeoPoint(33821899, -85763748));
		parking.get(parking.size() - 1).add(new GeoPoint(33821995, -85762764));
		parking.get(parking.size() - 1).add(new GeoPoint(33822227, -85762848));
		parking.get(parking.size() - 1).add(new GeoPoint(33822041, -85763786));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33825329, -85768753));
		parking.get(parking.size() - 1).add(new GeoPoint(33825333, -85769188));
		parking.get(parking.size() - 1).add(new GeoPoint(33825695, -85769180));
		parking.get(parking.size() - 1).add(new GeoPoint(33825687, -85768745));
		parking.get(parking.size() - 1).add(new GeoPoint(33825329, -85768753));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33825420, -85768555));
		parking.get(parking.size() - 1).add(new GeoPoint(33825321, -85768555));
		parking.get(parking.size() - 1).add(new GeoPoint(33825317, -85768341));
		parking.get(parking.size() - 1).add(new GeoPoint(33825420, -85768341));
		parking.get(parking.size() - 1).add(new GeoPoint(33825420, -85768555));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33823330, -85769958));
		parking.get(parking.size() - 1).add(new GeoPoint(33823097, -85769958));
		parking.get(parking.size() - 1).add(new GeoPoint(33823101, -85769531));
		parking.get(parking.size() - 1).add(new GeoPoint(33823330, -85769547));
		parking.get(parking.size() - 1).add(new GeoPoint(33823330, -85769958));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33820251, -85767952));
		parking.get(parking.size() - 1).add(new GeoPoint(33820412, -85767548));
		parking.get(parking.size() - 1).add(new GeoPoint(33820160, -85767586));
		parking.get(parking.size() - 1).add(new GeoPoint(33820251, -85767952));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33819748, -85764534));
		parking.get(parking.size() - 1).add(new GeoPoint(33819576, -85764519));
		parking.get(parking.size() - 1).add(new GeoPoint(33819607, -85764000));
		parking.get(parking.size() - 1).add(new GeoPoint(33819778, -85764015));
		parking.get(parking.size() - 1).add(new GeoPoint(33819748, -85764534));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33823891, -85768631));
		parking.get(parking.size() - 1).add(new GeoPoint(33823887, -85768227));
		parking.get(parking.size() - 1).add(new GeoPoint(33824364, -85768234));
		parking.get(parking.size() - 1).add(new GeoPoint(33824368, -85768639));
		parking.get(parking.size() - 1).add(new GeoPoint(33823891, -85768631));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33824055, -85766663));
		parking.get(parking.size() - 1).add(new GeoPoint(33824036, -85766777));
		parking.get(parking.size() - 1).add(new GeoPoint(33824211, -85766853));
		parking.get(parking.size() - 1).add(new GeoPoint(33824295, -85767029));
		parking.get(parking.size() - 1).add(new GeoPoint(33824364, -85766968));
		parking.get(parking.size() - 1).add(new GeoPoint(33824272, -85766762));
		parking.get(parking.size() - 1).add(new GeoPoint(33824055, -85766663));

		return parking;
	}
	
	public static ArrayList<ArrayList<GeoPoint>> getOrangeParking(){
		ArrayList<ArrayList<GeoPoint>> parking = new ArrayList<ArrayList<GeoPoint>>();
		
		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33821552, -85777374));
		parking.get(parking.size() - 1).add(new GeoPoint(33821548, -85777252));
		parking.get(parking.size() - 1).add(new GeoPoint(33821693, -85777252));
		parking.get(parking.size() - 1).add(new GeoPoint(33821686, -85777054));
		parking.get(parking.size() - 1).add(new GeoPoint(33822056, -85777054));
		parking.get(parking.size() - 1).add(new GeoPoint(33822063, -85777275));
		parking.get(parking.size() - 1).add(new GeoPoint(33821693, -85777252));
		parking.get(parking.size() - 1).add(new GeoPoint(33821697, -85777374));
		parking.get(parking.size() - 1).add(new GeoPoint(33821552, -85777374));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33822052, -85776794));
		parking.get(parking.size() - 1).add(new GeoPoint(33821697, -85776794));
		parking.get(parking.size() - 1).add(new GeoPoint(33821697, -85776680));
		parking.get(parking.size() - 1).add(new GeoPoint(33822048, -85776680));
		parking.get(parking.size() - 1).add(new GeoPoint(33822052, -85776794));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33822357, -85772324));
		parking.get(parking.size() - 1).add(new GeoPoint(33822350, -85772072));
		parking.get(parking.size() - 1).add(new GeoPoint(33823559, -85772064));
		parking.get(parking.size() - 1).add(new GeoPoint(33823566, -85772301));
		parking.get(parking.size() - 1).add(new GeoPoint(33822357, -85772324));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33822044, -85772346));
		parking.get(parking.size() - 1).add(new GeoPoint(33822041, -85772087));
		parking.get(parking.size() - 1).add(new GeoPoint(33821411, -85772079));
		parking.get(parking.size() - 1).add(new GeoPoint(33821259, -85772011));
		parking.get(parking.size() - 1).add(new GeoPoint(33821171, -85771843));
		parking.get(parking.size() - 1).add(new GeoPoint(33821156, -85771599));
		parking.get(parking.size() - 1).add(new GeoPoint(33821175, -85770882));
		parking.get(parking.size() - 1).add(new GeoPoint(33821220, -85770645));
		parking.get(parking.size() - 1).add(new GeoPoint(33821323, -85770508));
		parking.get(parking.size() - 1).add(new GeoPoint(33821457, -85770439));
		parking.get(parking.size() - 1).add(new GeoPoint(33821907, -85770432));
		parking.get(parking.size() - 1).add(new GeoPoint(33822067, -85770432));
		parking.get(parking.size() - 1).add(new GeoPoint(33822063, -85770164));
		parking.get(parking.size() - 1).add(new GeoPoint(33821472, -85770180));
		parking.get(parking.size() - 1).add(new GeoPoint(33821239, -85770264));
		parking.get(parking.size() - 1).add(new GeoPoint(33821068, -85770462));
		parking.get(parking.size() - 1).add(new GeoPoint(33821007, -85770599));
		parking.get(parking.size() - 1).add(new GeoPoint(33820984, -85770828));
		parking.get(parking.size() - 1).add(new GeoPoint(33820957, -85771469));
		parking.get(parking.size() - 1).add(new GeoPoint(33820965, -85771767));
		parking.get(parking.size() - 1).add(new GeoPoint(33821011, -85771973));
		parking.get(parking.size() - 1).add(new GeoPoint(33821079, -85772141));
		parking.get(parking.size() - 1).add(new GeoPoint(33821217, -85772263));
		parking.get(parking.size() - 1).add(new GeoPoint(33821365, -85772324));
		parking.get(parking.size() - 1).add(new GeoPoint(33822044, -85772346));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33822315, -85770424));
		parking.get(parking.size() - 1).add(new GeoPoint(33822308, -85770157));
		parking.get(parking.size() - 1).add(new GeoPoint(33823051, -85770164));
		parking.get(parking.size() - 1).add(new GeoPoint(33823051, -85770432));
		parking.get(parking.size() - 1).add(new GeoPoint(33822315, -85770424));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33823566, -85772041));
		parking.get(parking.size() - 1).add(new GeoPoint(33823498, -85772041));
		parking.get(parking.size() - 1).add(new GeoPoint(33823406, -85771461));
		parking.get(parking.size() - 1).add(new GeoPoint(33823326, -85771095));
		parking.get(parking.size() - 1).add(new GeoPoint(33823284, -85770576));
		parking.get(parking.size() - 1).add(new GeoPoint(33823280, -85770393));
		parking.get(parking.size() - 1).add(new GeoPoint(33823353, -85770393));
		parking.get(parking.size() - 1).add(new GeoPoint(33823372, -85770790));
		parking.get(parking.size() - 1).add(new GeoPoint(33823414, -85771095));
		parking.get(parking.size() - 1).add(new GeoPoint(33823471, -85771408));
		parking.get(parking.size() - 1).add(new GeoPoint(33823536, -85771805));
		parking.get(parking.size() - 1).add(new GeoPoint(33823566, -85772041));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33819832, -85771271));
		parking.get(parking.size() - 1).add(new GeoPoint(33819340, -85771271));
		parking.get(parking.size() - 1).add(new GeoPoint(33819340, -85771049));
		parking.get(parking.size() - 1).add(new GeoPoint(33819828, -85771049));
		parking.get(parking.size() - 1).add(new GeoPoint(33819832, -85771271));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33819588, -85769753));
		parking.get(parking.size() - 1).add(new GeoPoint(33819115, -85769768));
		parking.get(parking.size() - 1).add(new GeoPoint(33819111, -85769615));
		parking.get(parking.size() - 1).add(new GeoPoint(33819481, -85769608));
		parking.get(parking.size() - 1).add(new GeoPoint(33819550, -85769608));
		parking.get(parking.size() - 1).add(new GeoPoint(33819546, -85769417));
		parking.get(parking.size() - 1).add(new GeoPoint(33819012, -85769417));
		parking.get(parking.size() - 1).add(new GeoPoint(33819008, -85769318));
		parking.get(parking.size() - 1).add(new GeoPoint(33819401, -85769302));
		parking.get(parking.size() - 1).add(new GeoPoint(33819405, -85768913));
		parking.get(parking.size() - 1).add(new GeoPoint(33819572, -85768913));
		parking.get(parking.size() - 1).add(new GeoPoint(33819588, -85769753));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33818607, -85767593));
		parking.get(parking.size() - 1).add(new GeoPoint(33819447, -85767616));
		parking.get(parking.size() - 1).add(new GeoPoint(33819313, -85765198));
		parking.get(parking.size() - 1).add(new GeoPoint(33818947, -85765190));
		parking.get(parking.size() - 1).add(new GeoPoint(33818928, -85764694));
		parking.get(parking.size() - 1).add(new GeoPoint(33818588, -85764664));
		parking.get(parking.size() - 1).add(new GeoPoint(33818607, -85767593));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33818600, -85762962));
		parking.get(parking.size() - 1).add(new GeoPoint(33818615, -85762863));
		parking.get(parking.size() - 1).add(new GeoPoint(33818913, -85762939));
		parking.get(parking.size() - 1).add(new GeoPoint(33818932, -85762939));
		parking.get(parking.size() - 1).add(new GeoPoint(33819023, -85762505));
		parking.get(parking.size() - 1).add(new GeoPoint(33818676, -85762398));
		parking.get(parking.size() - 1).add(new GeoPoint(33818703, -85762268));
		parking.get(parking.size() - 1).add(new GeoPoint(33819134, -85762428));
		parking.get(parking.size() - 1).add(new GeoPoint(33819023, -85763054));
		parking.get(parking.size() - 1).add(new GeoPoint(33818600, -85762962));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33819748, -85764534));
		parking.get(parking.size() - 1).add(new GeoPoint(33819778, -85764015));
		parking.get(parking.size() - 1).add(new GeoPoint(33820450, -85764107));
		parking.get(parking.size() - 1).add(new GeoPoint(33820415, -85764595));
		parking.get(parking.size() - 1).add(new GeoPoint(33819748, -85764534));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33819958, -85767578));
		parking.get(parking.size() - 1).add(new GeoPoint(33819962, -85768631));
		parking.get(parking.size() - 1).add(new GeoPoint(33820419, -85768578));
		parking.get(parking.size() - 1).add(new GeoPoint(33820812, -85767761));
		parking.get(parking.size() - 1).add(new GeoPoint(33820843, -85767738));
		parking.get(parking.size() - 1).add(new GeoPoint(33820923, -85767815));
		parking.get(parking.size() - 1).add(new GeoPoint(33821129, -85767326));
		parking.get(parking.size() - 1).add(new GeoPoint(33821030, -85767075));
		parking.get(parking.size() - 1).add(new GeoPoint(33820877, -85767487));
		parking.get(parking.size() - 1).add(new GeoPoint(33820415, -85767540));
		parking.get(parking.size() - 1).add(new GeoPoint(33820251, -85767975));
		parking.get(parking.size() - 1).add(new GeoPoint(33820152, -85767586));
		parking.get(parking.size() - 1).add(new GeoPoint(33819958, -85767578));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33821762, -85768654));
		parking.get(parking.size() - 1).add(new GeoPoint(33821430, -85768364));
		parking.get(parking.size() - 1).add(new GeoPoint(33820995, -85768410));
		parking.get(parking.size() - 1).add(new GeoPoint(33820881, -85768692));
		parking.get(parking.size() - 1).add(new GeoPoint(33820854, -85768562));
		parking.get(parking.size() - 1).add(new GeoPoint(33820877, -85768417));
		parking.get(parking.size() - 1).add(new GeoPoint(33820999, -85768143));
		parking.get(parking.size() - 1).add(new GeoPoint(33821224, -85768120));
		parking.get(parking.size() - 1).add(new GeoPoint(33821457, -85768135));
		parking.get(parking.size() - 1).add(new GeoPoint(33821621, -85768234));
		parking.get(parking.size() - 1).add(new GeoPoint(33821739, -85768349));
		parking.get(parking.size() - 1).add(new GeoPoint(33821770, -85768501));
		parking.get(parking.size() - 1).add(new GeoPoint(33821762, -85768654));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33821892, -85768669));
		parking.get(parking.size() - 1).add(new GeoPoint(33821960, -85768402));
		parking.get(parking.size() - 1).add(new GeoPoint(33822716, -85768585));
		parking.get(parking.size() - 1).add(new GeoPoint(33822716, -85768677));
		parking.get(parking.size() - 1).add(new GeoPoint(33821892, -85768669));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33823254, -85762398));
		parking.get(parking.size() - 1).add(new GeoPoint(33823143, -85762329));
		parking.get(parking.size() - 1).add(new GeoPoint(33823277, -85761894));
		parking.get(parking.size() - 1).add(new GeoPoint(33823399, -85761963));
		parking.get(parking.size() - 1).add(new GeoPoint(33823254, -85762398));
		
		return parking;
	}
	
	public static ArrayList<ArrayList<GeoPoint>> getGreenParking(){
		ArrayList<ArrayList<GeoPoint>> parking = new ArrayList<ArrayList<GeoPoint>>();

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33824493, -85768555));
		parking.get(parking.size() - 1).add(new GeoPoint(33824493, -85768654));
		parking.get(parking.size() - 1).add(new GeoPoint(33825180, -85768631));
		parking.get(parking.size() - 1).add(new GeoPoint(33825176, -85768547));
		parking.get(parking.size() - 1).add(new GeoPoint(33824493, -85768555));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33824486, -85768288));
		parking.get(parking.size() - 1).add(new GeoPoint(33824486, -85768066));
		parking.get(parking.size() - 1).add(new GeoPoint(33825165, -85768066));
		parking.get(parking.size() - 1).add(new GeoPoint(33825176, -85768303));
		parking.get(parking.size() - 1).add(new GeoPoint(33824486, -85768288));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33825417, -85767952));
		parking.get(parking.size() - 1).add(new GeoPoint(33825726, -85767952));
		parking.get(parking.size() - 1).add(new GeoPoint(33825722, -85766853));
		parking.get(parking.size() - 1).add(new GeoPoint(33825405, -85766861));
		parking.get(parking.size() - 1).add(new GeoPoint(33825417, -85767952));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33825199, -85767822));
		parking.get(parking.size() - 1).add(new GeoPoint(33824463, -85767807));
		parking.get(parking.size() - 1).add(new GeoPoint(33824448, -85767044));
		parking.get(parking.size() - 1).add(new GeoPoint(33824505, -85766747));
		parking.get(parking.size() - 1).add(new GeoPoint(33825188, -85766731));
		parking.get(parking.size() - 1).add(new GeoPoint(33825199, -85767822));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33824883, -85765182));
		parking.get(parking.size() - 1).add(new GeoPoint(33824951, -85764969));
		parking.get(parking.size() - 1).add(new GeoPoint(33825626, -85764969));
		parking.get(parking.size() - 1).add(new GeoPoint(33825535, -85765175));
		parking.get(parking.size() - 1).add(new GeoPoint(33824883, -85765182));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33825661, -85766373));
		parking.get(parking.size() - 1).add(new GeoPoint(33825417, -85766380));
		parking.get(parking.size() - 1).add(new GeoPoint(33825405, -85765701));
		parking.get(parking.size() - 1).add(new GeoPoint(33825245, -85765701));
		parking.get(parking.size() - 1).add(new GeoPoint(33825241, -85765907));
		parking.get(parking.size() - 1).add(new GeoPoint(33825100, -85765915));
		parking.get(parking.size() - 1).add(new GeoPoint(33825108, -85766472));
		parking.get(parking.size() - 1).add(new GeoPoint(33824619, -85766510));
		parking.get(parking.size() - 1).add(new GeoPoint(33824631, -85766251));
		parking.get(parking.size() - 1).add(new GeoPoint(33824718, -85765961));
		parking.get(parking.size() - 1).add(new GeoPoint(33824726, -85765472));
		parking.get(parking.size() - 1).add(new GeoPoint(33825649, -85765495));
		parking.get(parking.size() - 1).add(new GeoPoint(33825661, -85766373));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33825581, -85764633));
		parking.get(parking.size() - 1).add(new GeoPoint(33825695, -85764427));
		parking.get(parking.size() - 1).add(new GeoPoint(33825546, -85764252));
		parking.get(parking.size() - 1).add(new GeoPoint(33825199, -85764206));
		parking.get(parking.size() - 1).add(new GeoPoint(33824986, -85764084));
		parking.get(parking.size() - 1).add(new GeoPoint(33824986, -85764359));
		parking.get(parking.size() - 1).add(new GeoPoint(33825180, -85764626));
		parking.get(parking.size() - 1).add(new GeoPoint(33825581, -85764633));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33823269, -85767761));
		parking.get(parking.size() - 1).add(new GeoPoint(33823166, -85767860));
		parking.get(parking.size() - 1).add(new GeoPoint(33823017, -85767929));
		parking.get(parking.size() - 1).add(new GeoPoint(33822453, -85767456));
		parking.get(parking.size() - 1).add(new GeoPoint(33822594, -85767220));
		parking.get(parking.size() - 1).add(new GeoPoint(33823029, -85767532));
		parking.get(parking.size() - 1).add(new GeoPoint(33823429, -85767372));
		parking.get(parking.size() - 1).add(new GeoPoint(33823345, -85767647));
		parking.get(parking.size() - 1).add(new GeoPoint(33823269, -85767761));

		parking.add(new ArrayList<GeoPoint>());
		parking.get(parking.size() - 1).add(new GeoPoint(33824413, -85768188));
		parking.get(parking.size() - 1).add(new GeoPoint(33824337, -85768188));
		parking.get(parking.size() - 1).add(new GeoPoint(33824318, -85767334));
		parking.get(parking.size() - 1).add(new GeoPoint(33824398, -85767334));
		parking.get(parking.size() - 1).add(new GeoPoint(33824413, -85768188));

		return parking;
	}
}
