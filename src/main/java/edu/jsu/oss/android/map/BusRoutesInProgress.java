/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class BusRoutesInProgress {
		
	private final int school_end = 121;
	private final int school_start = 227;
	
	public boolean red;
	public boolean white;
	public boolean purple;
	public boolean blue;
	public boolean black;
	public boolean yellow;
	public boolean green;
	
	public boolean hasRoutes;

	public BusRoutesInProgress(){
		
		//SimpleTimeZone centralTime = new SimpleTimeZone( -6 * 60 * 60 * 1000, "Central");
		
		GregorianCalendar calendar = new GregorianCalendar();
		
		hasRoutes = false; //remains false until set to true
		
		if(calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
			
			//check to determine if it is summer
	    	if (calendar.get(Calendar.DAY_OF_YEAR) < school_end || calendar.get(Calendar.DAY_OF_YEAR) > school_start){
	    		//summer routes off
	    		green = false;
	    		
	    		//weekday routes are determined by hour:
	    		switch(calendar.get(Calendar.HOUR_OF_DAY)){
    			case 7:
    			case 8:
    			case 9:
    				red = true;
    	    		white = true;
    	    		purple = true;
    	    		blue = false;
    	    		black = false;
    	    		hasRoutes = true;
    				break;
    			case 10:
    			case 11:
    			case 12:
    			case 13:
    				red = true;
    	    		white = true;
    	    		purple = true;
    	    		blue = true;
    	    		black = false;
    	    		hasRoutes = true;
    				break;
    			case 14:
    			case 15:
    			case 16:
    				red = true;
    	    		white = true;
    	    		purple = false;
    	    		blue = true;
    	    		black = false;
    	    		hasRoutes = true;
    				break;
    			case 17:
    			case 18:
    			case 19:
    			case 20:
    				red = true;
    	    		white = true;
    	    		purple = false;
    	    		blue = false;
    	    		black = true;
    	    		hasRoutes = true;
    				break;
    			default:
    				red = false;
    	    		white = false;
    	    		purple = false;
    	    		blue = false;
    	    		black = false;
    	    		break;
    			}
	    		
	    		//Yellow is determined by DOW
	    		if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY){ //it is Mon-Wed
	    			if (calendar.get(Calendar.HOUR_OF_DAY) < 3 || calendar.get(Calendar.HOUR_OF_DAY) > 21){
	    				yellow = true;
	    				hasRoutes = true;
	    			}
	    			else
	    				yellow = false;
	    		}
	    		else{
	    			yellow = false;
	    		}	    		
	    	}
	    	else{ // it is summer
	    		red = false;
	    		white = false;
	    		purple = false;
	    		blue = false;
	    		black = false;
	    		yellow = false;
	    		
	    		//If the hours are correct, green is running
	    		if (calendar.get(Calendar.HOUR_OF_DAY) < 7 || calendar.get(Calendar.HOUR_OF_DAY) > 21){
	    			green = true;
	    			hasRoutes = true;
	    		}
	    		else
	    			green = false;
	    	}
	    }
	    else{ //it's the weekend, no buses run
	    	red = false;
    		white = false;
    		purple = false;
    		blue = false;
    		black = false;
    		yellow = false;
    		green = false;
	    }
	}
}
