/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;

import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
import com.google.android.maps.MapView;

public class BusRouteOverlay extends Overlay {
	
	private boolean busesRunning;
	private ArrayList<BusRoute> routes;
	
	public BusRouteOverlay(){
		BusRoutesInProgress inProgress = new BusRoutesInProgress();
		
		busesRunning = inProgress.hasRoutes;
		
		routes = new ArrayList<BusRoute>();
		
		//Assign properties to each route in progress
		if(busesRunning){
			if(inProgress.red)
				routes.add(new BusRoute(BusRouteData.getRedLine(), BusRouteData.getRedStops(), BusRouteData.getRedColor()));
			if(inProgress.white)
				routes.add(new BusRoute(BusRouteData.getWhiteLine(), BusRouteData.getWhiteStops(), BusRouteData.getWhiteColor()));
			if(inProgress.purple)
				routes.add(new BusRoute(BusRouteData.getPurpleLine(), BusRouteData.getPurpleStops(), BusRouteData.getPurpleColor()));
			if(inProgress.blue)
				routes.add(new BusRoute(BusRouteData.getBlueLine(), BusRouteData.getBlueStops(), BusRouteData.getBlueColor()));
			if(inProgress.black)
				routes.add(new BusRoute(BusRouteData.getBlackLine(), BusRouteData.getBlackStops(), BusRouteData.getBlackColor()));
			if(inProgress.yellow)
				routes.add(new BusRoute(BusRouteData.getYellowLine(), BusRouteData.getYellowStops(), BusRouteData.getYellowColor()));
			if(inProgress.green)
				routes.add(new BusRoute(BusRouteData.getGreenLine(), BusRouteData.getGreenStops(), BusRouteData.getGreenColor()));
		}
	}
	
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow)
	{
		int zoom = mapView.getZoomLevel();
		
		//only bother drawing if there are buses running
		if(busesRunning){
			if (zoom > 16){
				Projection projection = mapView.getProjection();
			    Point point = new Point();
			    
			    //define the paint
			    Paint routePaint = new Paint();
			    routePaint.setStrokeWidth((zoom%15)*2);
			    routePaint.setAntiAlias(true);
			    
			    //Draw the bus routes
			    for (int i = 0; i < routes.size(); i++){ //Route iterator (i)
			    	//set paint to the color of the route
			    	routePaint.setColor(routes.get(i).color);
					Path p = new Path();//**just moved**
			    	
	
				    routePaint.setStyle(Style.STROKE);
				    for (int j = 0; j < routes.get(i).lines.size(); j++){ //GeoPoint iterator (j)
					    
				    	projection.toPixels(routes.get(i).lines.get(j), point);
					    
					    if (j != 0)
						    p.lineTo(point.x, point.y);
					    else
					    	p.moveTo(point.x, point.y);
				    }
				    
				    canvas.drawPath(p, routePaint);
				    
				    if (zoom > 17){
					    // Now Draw the bus stops
					    routePaint.setStyle(Style.FILL);
					    for (int k = 0; k < routes.get(i).stops.size(); k++){
					    	
					    	projection.toPixels(routes.get(i).stops.get(k), point);
					    	
					    	//draw the circle
					    	canvas.drawCircle(point.x, point.y, (zoom%17)*7, routePaint);			    	
					    }
				    }
		    	}
		    }
		} //end of draw Override	    
	}
}
