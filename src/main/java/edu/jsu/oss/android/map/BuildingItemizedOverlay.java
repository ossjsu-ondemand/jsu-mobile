/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.lang.reflect.Method;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import edu.jsu.oss.android.R;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapView;
import com.google.android.maps.MapController;
import com.google.android.maps.GeoPoint;

/**
 * ItemizedOverlay class consisting of BuildingOverlayItem elements that represent buildings on campus.  This
 * building list is meant to grow and shrink as the user zooms in and out so that icons do not overlap and clutter
 * up the display.
 *
 */
public class BuildingItemizedOverlay extends ItemizedOverlay<BuildingOverlayItem> {

	/** Indicates zoom level at which only the "largest" icons are visible.  This zoom level will show the 
	 * least amount of icons and beyond this no icons will be visible. */
	private final int large_zoom = 17;
	/** Indicates zoom level at which the "medium" and "large" icons are visible. */
	private final int medium_zoom = 18;
	/** Indicates zoom level at which all icons are visible.  This is also the maximum zoom level reached by
	 * the campus map. */
	private final int small_zoom = 19;
	
	/** List of all the buildings on campus that are to be represented as icons.  This list will grow 
	 * and shrink as the zoom level changes. */
	private static ArrayList<BuildingOverlayItem> items;
	/** Context in which the overlay is displayed. */
	private Context context;
	/** Current zoom level of the MapView. */
	private int zoomLevel;
	
	/** MapView in which this overlay is placed. */
	private MapView mapView;
	/** View that is responsible for drawing the balloons with a particular building's title and description. */
	private BalloonOverlayView balloonView;
	/** Used to indicate the clickable area of each icon in this overlay. */
	private View clickRegion;
	/** Offset (in pixels) between the overlay and the map.  This will not be anything other than 0 under
	 * normal circumstances. */
	private int viewOffset;
	/** Controller used to zoom and pan the map. */
	final MapController mc;
	
	public BuildingItemizedOverlay(Context context, Drawable defaultMarker, int zoomLevel, MapView mapView){
		super(boundCenter(defaultMarker));
		this.context = context;
		this.zoomLevel = zoomLevel;
		this.mapView = mapView;
		viewOffset = 0;
		mc = mapView.getController();
		
		//load the appropriate set of list items for the zoom level
		switch(zoomLevel){
			case large_zoom:
				items = getLargeIconArrayList();
				break;
			case medium_zoom:
				items = getMediumIconArrayList();
				break;
			case small_zoom:
				getSmallIconArrayList();
				break;
		}			
		
		populate();
	}
	
	@Override
	protected BuildingOverlayItem createItem(int index){
		return items.get(index);
	}
	
	@Override
	public int size(){
		return items.size();
	}
	
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow){
		int currZoom = mapView.getZoomLevel();
		
		//be sure not to zoom out past what is allowed
		if (currZoom < 15)
			mapView.getController().setZoom(15);
		else{
			if (zoomLevel != currZoom){ //if the zoom has changed
				
				//clear all current items, they must be re-drawn
                items.clear(); 
                setLastFocusedIndex(-1);
                
                //Turn any balloons off
                hideBalloon();
                				
				switch (mapView.getZoomLevel()){
					case large_zoom:
						items = getLargeIconArrayList();
						populate();
						break;
					case medium_zoom:
						items = getMediumIconArrayList();
						populate();
						break;
					case small_zoom:
						items = getSmallIconArrayList();
						populate();
						break;
					default: //When it is zoomed out too far, empty the list so the onTap listener doesn't freak out
						items = new ArrayList<BuildingOverlayItem>();
						populate();
				} 
				zoomLevel = currZoom;
			}
			
			if(zoomLevel >= large_zoom)
				super.draw(canvas, mapView, false);
		}
	}
	
	@Override
	public boolean onTap(int index){
		boolean isRecycled;
		final int thisIndex;
		GeoPoint point;
		
		thisIndex = index;
		point = createItem(index).getPoint();
		
		if (balloonView == null) {
			balloonView = new BalloonOverlayView(mapView.getContext(), viewOffset);
			clickRegion = (View) balloonView.findViewById(R.id.balloon_inner_layout);
			isRecycled = false;
		} else {
			isRecycled = true;
		}
	
		balloonView.setVisibility(View.GONE);
		
		balloonView.setData(createItem(index));
		
		MapView.LayoutParams params = new MapView.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, point,
				MapView.LayoutParams.BOTTOM_CENTER);
		params.mode = MapView.LayoutParams.MODE_MAP;
		
		setBalloonTouchListener(thisIndex);
		if (isRecycled) {
			balloonView.setLayoutParams(params);
		} else {
			mapView.addView(balloonView, params);
		}
		
		balloonView.setVisibility(View.VISIBLE);
		
		mc.animateTo(point);
		
		return true;
	}
	
	/**
	 * Set the horizontal distance between the marker and the bottom of the information
	 * balloon. The default is 0 which works well for center bounded markers. If your
	 * marker is center-bottom bounded, call this before adding overlay items to ensure
	 * the balloon hovers exactly above the marker. 
	 * 
	 * @param pixels - The padding between the center point and the bottom of the
	 * information balloon.
	 */
	public void setBalloonBottomOffset(int pixels) {
		viewOffset = pixels;
	}
	
	/**
	 * Override this method to handle a "tap" on a balloon. By default, does nothing 
	 * and returns false.
	 * 
	 * @param index - The index of the item whose balloon is tapped.
	 * @return true if you handled the tap, otherwise false.
	 */
	protected boolean onBalloonTap(int index) {
		return false;
	}
	
	/**
	 * Sets the visibility of this overlay's balloon view to GONE. 
	 */
	protected void hideBalloon() {
		if (balloonView != null) {
			balloonView.setVisibility(View.GONE);
		}
	}
	
	/**
	 * Sets the onTouchListener for the balloon being displayed, calling the
	 * overridden onBalloonTap if implemented.
	 * 
	 * @param thisIndex - The index of the item whose balloon is tapped.
	 */
	private void setBalloonTouchListener(final int thisIndex) {
		
		try {
			@SuppressWarnings("unused")
			Method m = this.getClass().getDeclaredMethod("onBalloonTap", int.class);
			
			clickRegion.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					
					View l =  ((View) v.getParent()).findViewById(R.id.balloon_main_layout);
					Drawable d = l.getBackground();
					
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						int[] states = {android.R.attr.state_pressed};
						if (d.setState(states)) {
							d.invalidateSelf();
						}
						return true;
					} else if (event.getAction() == MotionEvent.ACTION_UP) {
						int newStates[] = {};
						if (d.setState(newStates)) {
							d.invalidateSelf();
						}
						// call overridden method
						onBalloonTap(thisIndex);
						return true;
					} else {
						return false;
					}
					
				}
			});
			
		} catch (SecurityException e) {
			//Log.e("BalloonItemizedOverlay", "setBalloonTouchListener reflection SecurityException");
			return;
		} catch (NoSuchMethodException e) {
			// method not overridden - do nothing
			return;
		}

	}			
	
	//This cannot be moved to an external class because the context is needed to retrieve the drawables
	//wish I could make it static, but since the list has to be drawn dynamically and drawables can't be 
	//referenced outside of activity context it must be dynamic
	private ArrayList<BuildingOverlayItem> getLargeIconArrayList(){
		ArrayList<BuildingOverlayItem> list = new ArrayList<BuildingOverlayItem>();
		
		list.add(new BuildingOverlayItem(new GeoPoint(33823391, -85766693), "Ramona Wood Hall", "The Romana Wood Building is the home of the College of Education and Professional Studies.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33824089, -85765823), "Ayers Hall", "Ayers Hall is home to Computer Science, Mathematics, Technology, and Psychology.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33823799, -85767006), "Sparkman Hall", "Standing nine stories tall, this Co-ed Dormitory is a landmark on campus.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33821911, -85764282), "Theron Montgomery Building - TMB", "More often referred to as the &quot;TMB,&quot; this student commons building houses the mail center, tutoring services, print shop, and office of student activities.", boundCenter(context.getResources().getDrawable(R.drawable.map_university))));
		list.add(new BuildingOverlayItem(new GeoPoint(33818947, -85762718), "Colonial Arms Apartments", "one and two bedroom apartments requiring sophomore status for application.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33819695, -85766266), "Stadium Tower Dormitory", "Four bedroom suite with kitchen, common area, and two bathrooms.  Requires Sophomore status for residency application.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33820343, -85766457), "Burgess-Snow Football Field", "Home of Gamecock Football, the 24,000 seat facility is also used for graduation and other special events.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		list.add(new BuildingOverlayItem(new GeoPoint(33821392, -85767029), "Fitzpatrick Hall Dormitory", "Female freshmen dormitory with double occupancy rooms and community baths.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33820717, -85768181), "Curtiss Hall Dormitory", "Female freshmen dormitory for those interested in Greek life.  Curtiss Hall has double occupancy rooms and community baths.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33819538, -85769371), "Campus Inn Apartments", "Efficiency and one bedroom apartments requiring sophomore status at time of application.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33819561, -85771141), "Jax Apartments", "One and two bedroom unfurnished apartments for families and married couples.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33821793, -85776939), "Penn House Apartments", "One bedroom apartments requiring sophomore status at the time of application.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822704, -85775696), "Gamecock Track", "Standard 400 yd track with fully equipped infield.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		list.add(new BuildingOverlayItem(new GeoPoint(33825050, -85776596), "Rudy Abbott Baseball Field", "Home of the two-time national champion Gamecock Baseball team.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		list.add(new BuildingOverlayItem(new GeoPoint(33824982, -85775032), "JSU Soccer Field", "Built in 2003, this is the women's soccer facility.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		list.add(new BuildingOverlayItem(new GeoPoint(33824093, -85773598), "Pete Matthews Coliseum", "Functions as the arena for both men's and women's basketball in addition to housing an indoor pool, weight facilities, and exercise classes.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822346, -85767487), "Pannell Hall Apartments", "Efficiency apartments conveniently located in the center of campus.  These require sophomore status at the time of application.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33823090, -85768364), "Mason Hall", "Facilities for the Department of Music and Department of Family and Consumer Sciences.  Home of the Marching Southerner and Ballerinas, Mason Hall also features a concert hall.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33823662, -85768433), "Carlisle Fine Arts Facility", "Arts building with digital and film laboratories as well as painting, drawing, and sculpting studios.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33824833, -85768990), "Self Hall", "Houses the Department of Communications, Office of Distance Education, and TV Services.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33824875, -85768417), "Dixon Hall Dormitory", "Freshman male dormitory with double occupancy rooms and community baths.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33824875, -85767944), "Crow Hall Dormitory", "Freshman male dormitory with double occupancy rooms and community baths.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33825344, -85767540), "Patterson Hall Dormitory", "Male dormitory for \"Cocky Experience\" and honors students.  Double occupancy rooms with private bath.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33825333, -85766045), "Logan Hall", "Female dormitory for \"Cocky Experience\" and honors students.  Double occupancy rooms with private bath.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33825352, -85764763), "International House Dormitory", "Dormitory for the International House student program.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33826584, -85762672), "University Softball Field", "Home of Gamecock Softball with a seating capacity of 1,000 people.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		list.add(new BuildingOverlayItem(new GeoPoint(33826344, -85761513), "Wallace Hall", "Home to the renowned nursing program of JSU.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33828590, -85763245), "Merrill Hall", "Home to the College of Commerce and Business Administration", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33830250, -85760658), "Ernest Stone Center", "Houses the Art, History, English, Drama, and Music Departments as well as a production stage.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33827286, -85763504), "Brewer Hall", "Home to the Sociology and Social Work, Criminal Justine, and Political Science and Public Administration Departments.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33825275, -85763367), "Martin Hall", "Home for Biology, Chemistry, Physics, and other related sciences.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33824188, -85763382), "Houston Cole Library", "700,000 volume library with conference center, computer labs, and study floor.", boundCenter(context.getResources().getDrawable(R.drawable.map_university))));
		list.add(new BuildingOverlayItem(new GeoPoint(33823376, -85762215), "College Apartments", "One bedroom apartments requiring sophomore status at the time of application.", boundCenter(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33820854, -85764061), "Stephenson Hall", "Student recreation facility complete with basketball courts, weight rooms, pool tables, racquetball courts, ping-pong, and steam rooms.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		list.add(new BuildingOverlayItem(new GeoPoint(33820152, -85763557), "Kennamer Hall", "Athletic administration offices as well as varsity sport weight room and training facilities.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822887, -85773178), "JSU Tennis Complex", "Lighted complex with fifteen tennis courts.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822308, -85766563), "Jack Hopper Dining Hall", "Buffet-Style Cafeteria offering a variety of food including home-style entrees, pizza and pasta bar, salads, and deli subs.", boundCenter(context.getResources().getDrawable(R.drawable.map_dining))));
		list.add(new BuildingOverlayItem(new GeoPoint(33823044, -85765282), "Bibb Graves Hall", "Bibb Graves Hall is the University's administration building and one of the oldest facilities on the main campus.", boundCenter(context.getResources().getDrawable(R.drawable.map_university))));
		
		return list;
	}
	
	private ArrayList<BuildingOverlayItem> getMediumIconArrayList(){
		ArrayList<BuildingOverlayItem> list = getLargeIconArrayList();
		
		list.add(new BuildingOverlayItem(new GeoPoint(33821438, -85765610), "Daugette Hall", "Daugette Hall, one of JSU's earliest dormitories, has been converted to office use for the computer center and office of continuing education.", boundCenter(context.getResources().getDrawable(R.drawable.map_university))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822090, -85766769), "Leon Cole Auditorium", "Auditorium for assemblies, conferences, and luncheons.", boundCenter(context.getResources().getDrawable(R.drawable.map_university))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822124, -85764130), "Theron Montgomery Food Court", "Campus Chick-Fil-A,  Baja Fresh Mexican Grill, and 155 Degrees restaurants.  Flex dollars are always welcome here!", boundCenter(context.getResources().getDrawable(R.drawable.map_dining))));
		list.add(new BuildingOverlayItem(new GeoPoint(33819668, -85762138), "Wesley House", "Methodist college ministry with residence halls.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33823734, -85769279), "Duncan Maintenance Shop", "Service and repair building for University property.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33826180, -85763733), "Baptist Campus Ministries - BCM", "Baptist college ministry for students.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33820721, -85764603), "Gamecock Diner", "Flex dining facility for JSU students.", boundCenter(context.getResources().getDrawable(R.drawable.map_dining))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822842, -85770927), "Leadership House for Women", "Private rooms with community baths.", boundCenterBottom(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822964, -85771309), "Sigma Phi Epsilon Fraternity House", "", boundCenterBottom(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822865, -85771667), "Kappa Alpha Fraternity House", "", boundCenterBottom(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822517, -85771843), "Delta Chi Fraternity House", "", boundCenterBottom(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33821457, -85771690), "Pi Kappa Phi Fraternity House", "", boundCenterBottom(context.getResources().getDrawable(R.drawable.map_dorm))));
		list.add(new BuildingOverlayItem(new GeoPoint(33830257, -85762970), "Ampitheater", "Outdoor stage for JSU's drama and musical productions.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33826614, -85760040), "Rowe Hall", "Military Science department and ROTC program facility.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		list.add(new BuildingOverlayItem(new GeoPoint(33825459, -85762833), "McGee Science Center", "Science laboratory and research facility.", boundCenter(context.getResources().getDrawable(R.drawable.map_book))));
		
		return list;
	}
	
	private ArrayList<BuildingOverlayItem> getSmallIconArrayList(){
		ArrayList<BuildingOverlayItem> list = getMediumIconArrayList();

		list.add(new BuildingOverlayItem(new GeoPoint(33821999, -85765190), "Anders Hall Round House", "Commonly referred to as the \"Round House,\" this is a common meeting and conference facility.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822117, -85764885), "Hammond Hall", "Hammond Hall on University Circle is the home of JSU&#39;s Department of Art and the Hammond Hall Art Gallery.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33821812, -85764900), "Computer Center", "Home of university computer and network support staff.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822319, -85764244), "Campus Bookstore", "Offerings in course textbooks as well as school supplies and JSU apparel.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33823692, -85767540), "Student Health Center", "Offers free medical care to full-time students of JSU.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822147, -85766380), "WOW Cafe & Wingery", "Offers a wide variety of wings, wraps, chicken, and special sauces.", boundCenter(context.getResources().getDrawable(R.drawable.map_dining))));
		list.add(new BuildingOverlayItem(new GeoPoint(33824493, -85764267), "President's Home", "Dwelling of Dr. William Meehan", boundCenterBottom(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33824249, -85763100), "Jazzman's Cafe", "Coffee Shop and Cafe will provide the brain fuel for the late nights at the library!", boundCenter(context.getResources().getDrawable(R.drawable.map_dining))));
		list.add(new BuildingOverlayItem(new GeoPoint(33822731, -85762138), "Alumni House", "Facility used for social and cultural events by JSU or Alumni.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33820377, -85767303), "Gamecock Field House", "Football administrative offices, meetings rooms, locker, and equipment facilities.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		list.add(new BuildingOverlayItem(new GeoPoint(33820213, -85769196), "Salls Hall - Police Department", "University Police Department and English Language Institute", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33826134, -85764503), "Visitor Center", "Provides campus tours and general information about JSU.", boundCenter(context.getResources().getDrawable(R.drawable.map_placemark))));
		list.add(new BuildingOverlayItem(new GeoPoint(33826611, -85760361), "Rifle Range", "Facility for JSU's 3-time National Champion rifle team.", boundCenter(context.getResources().getDrawable(R.drawable.map_sports))));
		
		return list;
	}
	
}
