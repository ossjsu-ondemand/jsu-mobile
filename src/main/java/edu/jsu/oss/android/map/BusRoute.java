/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.util.ArrayList;
import com.google.android.maps.GeoPoint;

public class BusRoute {

	public ArrayList<GeoPoint> lines;
	public ArrayList<GeoPoint> stops;
	public int color;
	
	public BusRoute(ArrayList<GeoPoint> lines, ArrayList<GeoPoint> stops, int color){
		this.lines = lines;
		this.stops = stops;
		this.color = color;
	}
}
