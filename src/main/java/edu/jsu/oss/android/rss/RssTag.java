/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.rss;

/**
 * Static class used for generating HTML tags to be displayed in an RSS WebView.
 *
 */
public class RssTag {

	/**
	 * Returns all of the HTML style elements used for the content.
	 * 
	 * @return String containing all of the HTML style elements used for the content
	 */
	public static String openDocument(){
		StringBuilder s = new StringBuilder();
		s.append("<HEAD><STYLE type=\"text/css\">");
		s.append("body {background-color:#000; color:#FFF; font-family:arial, sans-serif; margin:0px; padding:0px;}");
		s.append("h2 {color:#E90000; font-size:1.5em; font-weight:bold; margin-top: .7em;}");
		s.append("h5 {color:#9A9A9A; font-size:.7em; font-style: italic;}");
		s.append("a, a:visited, a:active, a:hover {color:#E90000;}");
		s.append("</STYLE></HEAD><BODY>");
		
		return s.toString();
	}
	
	/**
	 * Formats the header text as HTML.
	 * 
	 * @param headerText text to be written as an HTML header
	 * @return headerText with HTML start/end tags
	 */
	public static String header(String headerText){
		return "<h2>" + headerText + "</h2>";
	}
	
	/**
	 * Formats the date as HTML.
	 * 
	 * @param dateText text to be written as HTML date
	 * @return dateText with HTML start/end tags
	 */
	public static String date(String dateText){
		return "<h5>" + dateText + "</h5>";
	}
	
	/**
	 * Returns the HTML closing tags for the document.
	 * 
	 * @return closing HTML tags
	 */
	public static String closeDocument(){
		return "<br /></BODY>";
	}
}
