/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.rss;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Event feedType for parsing an RSS feed.  Upon events fired, loads data into the given feed item.
 *
 */
public class RssHandler extends DefaultHandler {
	
	/** Static integer representing the state when an item is encountered, 
	 * but before any child elements are found. */
	private final int state_unknown = 0;
	/** Static integer representing the state when a title element is encountered. */
	private final int state_title = 1;
	/** Static integer representing the state when a link element is encountered. */
	private final int state_link = 2;
	/** Static integer representing the state when a publication date element is encountered.*/
	private final int state_pubdate = 3;
	
	/** The current parsing status, based on reader location.  This indicates what kind of element
	 * the parsing is currently located at. */
	private int currentState = state_unknown;
	
	/** The source RssFeed from which the article is to be retrieved. */
	public RssFeed feed;
	/** Feed list item containing basic information regarding the article to be retrieved. */
	private RssItem item;
	
	/** Flag indicated whether the current elements are related to an item.  If false, elements
	 * are known to be related to the feed properties. */
	boolean itemFound = false;
	/** Specifies whether the feed is for the Chanticleer or the News Feed. */
	int feedType;
	
	/**
	 * @param specifies whether the RSS feed is for the Chanticleer or News Feed.
	 */
	public RssHandler(int feedType){
		this.feedType = feedType;
	}
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#startDocument()
	 */
	@Override
	public void startDocument() throws SAXException {
		feed = new RssFeed(feedType);
		item = new RssItem();	
	}

	/**
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		//set flags based on start element tags
		if (localName.equalsIgnoreCase("item")){
			itemFound = true;
			item = new RssItem();
			currentState = state_unknown;
		}
		else if (localName.equalsIgnoreCase("title")){
			currentState = state_title;
		}
		else if (localName.equalsIgnoreCase("guid")){
			if(feedType == feed.news)
				currentState = state_link;
		}
		else if (localName.equalsIgnoreCase("link")){
			if(feedType == feed.chanty)
				currentState = state_link;
		}
		else if (localName.equalsIgnoreCase("pubdate")){
			currentState = state_pubdate;
		}
		else{
			currentState = state_unknown;
		}
		
	}
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		
		String strCharacters = new String(ch,start,length);
		
		//set item and feed properties based on events as the parser reads
		if (itemFound==true){
		// "item" tag found, it's item's parameter
			switch(currentState){
			case state_title:
				item.title += strCharacters;
				break;
			case state_link:
				item.link += strCharacters;
				break;
			case state_pubdate:
					item.pubdate = removeTime(strCharacters);
				break;	
			default:
				break;
			}
		}
		else{
		// not "item" tag found, it's feed's parameter
			switch(currentState){
			case state_title:
				feed.title = strCharacters;
				break;
			case state_link:
				feed.link = strCharacters;
				break;
			case state_pubdate:
				feed.pubdate = removeTime(strCharacters);
				break;	
			default:
				break;
			}
		}
	}
	
	/**
	 * Takes a string containing the standard date and time format and removes the time element.
	 *  
	 * @param dateTime contains both the publication date and the time the article was published.
	 * @return string stripped of the time, containing only the date the article was published.
	 */
	private String removeTime(String dateTime){
		int stopChar = dateTime.indexOf(":00 ") - 5;
		if(stopChar > 0)
			return dateTime.substring(0, stopChar);
		else
			return dateTime;
	}
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (localName.equalsIgnoreCase("item")){
			feed.itemList.add(item);
		}
		currentState = state_unknown;
	}
}
